#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
// WIN32 /////////////////////////////////////////
#ifdef __WIN32__

#include <windows.h>

// UNIX //////////////////////////////////////////
#else

#include <unistd.h>
#define Sleep( msec ) usleep(( msec ) * 1000 )

//////////////////////////////////////////////////
#endif
const char const *color[] = { "?", "BLACK", "BLUE", "GREEN", "YELLOW", "RED", "WHITE", "BROWN" };
#define COLOR_COUNT  (( int )( sizeof( color ) / sizeof( color[ 0 ])))
  //initialisation
  int portA=OUTPUT_A;
  int portD=OUTPUT_D;
  int portB=OUTPUT_B;
  int portC=OUTPUT_C;
  int chain_pos=0;
  float new_path=0;
  float old_D=0;
  float old_A=0;
  float value_gyro;
  float value_ultrasonic;
  int value_color; 
  uint8_t sn;
  char s[ 256 ];
  //uint8_t sn_base[0];//Left
  //uint8_t sn_base[1];//right
  uint8_t snB;//chain
  uint8_t snC;//kick
  uint8_t sn_base[2];
  uint8_t sn1;
  uint8_t sn2;//gyro
  uint8_t sn3;//UltraSonic
  uint8_t sn4;//ColorSensor
  FLAGS_T state;
  FLAGS_T stateA;
  FLAGS_T stateD;
  FLAGS_T stateB;
  FLAGS_T stateC;
  int object_identified;//0:ball  1:cube  2:triangle up  3:cylindre  3:triangle down
  int position_kick;
  int position_chain;
  float loc_y=25;//initial
  pthread_t thread_location_id; 
  pthread_t thread_find_object;
  float loc_x=50;//location for x and y
  bool found_obstacle=false;

//combined move function
//by Adib Rachid
void move(bool dist_not_time, int valueA,int valueD, int speedA, int speedD )
{
  //Run motors in order from port A to D
if(!dist_not_time){
      //motor A config
      set_tacho_stop_action_inx( sn_base[0], TACHO_COAST );
      set_tacho_speed_sp( sn_base[0], speedA);
      set_tacho_time_sp( sn_base[0], valueA );
      set_tacho_ramp_up_sp( sn_base[0], 2000 );
      set_tacho_ramp_down_sp( sn_base[0], 2000 );
      //motor D config
      set_tacho_stop_action_inx( sn_base[1], TACHO_COAST );
      set_tacho_speed_sp( sn_base[1], speedD);
      set_tacho_time_sp( sn_base[1], valueD );
      set_tacho_ramp_up_sp( sn_base[1], 2000 );
      set_tacho_ramp_down_sp( sn_base[1], 2000 );
      //START
      if(valueA*valueD>=0){
        multi_set_tacho_command_inx(sn_base, TACHO_RUN_TIMED);
      }else{
        set_tacho_command_inx( sn_base[0], TACHO_RUN_TIMED );
        set_tacho_command_inx( sn_base[1], TACHO_RUN_TIMED );
      }
    }
    else{
      //diameter of the wheel: 5cm -> R=4.5cm -> perimeter = 2* pi* R = 15.7  so 360 unite -> 15.7cm => 1cm=> 22.93
      int error= 0;
      error=ceil(valueA/150);//error after testing many times on diff. distances and speed values
      if(speedA>0)
        valueA=valueA*22.95-error;
      else
        valueA=valueA*22.95+error;
      if(speedD>0)
        valueD=valueD*22.95-error;
      else
        valueD=valueD*22.95+error; 
      //motor A config
      set_tacho_speed_sp( sn_base[0], speedA );
      set_tacho_ramp_up_sp( sn_base[0], 3000 );
      set_tacho_ramp_down_sp( sn_base[0], 3000 );
      set_tacho_position_sp( sn_base[0], valueA );
      //motor B config
      set_tacho_speed_sp( sn_base[1], speedD );
      set_tacho_ramp_up_sp( sn_base[1], 3000 );
      set_tacho_ramp_down_sp( sn_base[1], 3000 );
      set_tacho_position_sp( sn_base[1], valueD );
      //START   
      if(valueA*valueD>=0){
        multi_set_tacho_command_inx(sn_base, TACHO_RUN_TO_REL_POS);
      }else{
        set_tacho_command_inx( sn_base[0], TACHO_RUN_TO_REL_POS );
        set_tacho_command_inx( sn_base[1], TACHO_RUN_TO_REL_POS );
    }
  }

  /* Wait tacho stop */
  //Sleep( 1 );
  do {
    get_tacho_state_flags( sn_base[0], &stateA );
    get_tacho_state_flags( sn_base[1], &stateD );
  } while ( stateA || stateD );
}

//Move Backward
//by Adib Rachid
void backward(bool dist_not_time, int value, int speed ){
  move(dist_not_time,-value,-value,-speed,-speed );
}
//Move Forward
//by Adib Rachid
void forward(bool dist_not_time, int value, int speed ){
  move(dist_not_time,value,value,speed,speed );
}

//Turn Right
//by Adib Rachid
void turn_right(bool dist_not_time, int value, int speed ){
  move(dist_not_time,-value,value,-speed,speed );
}
//Turn Left
//by Adib Rachid
void turn_left(bool dist_not_time, int value, int speed ){
  move(dist_not_time,value,-value,speed,-speed );
}

//Turn Left Gyro -> left gyro value decreases
//by Adib Rachid
void turn_left_gyro(int angle,int speed){
  set_tacho_speed_sp( sn_base[0], -speed);
  set_tacho_speed_sp( sn_base[1], speed);
  //START

  get_sensor_value0(sn2, &value_gyro);
  if(value_gyro>angle){
    set_tacho_command_inx( sn_base[0], TACHO_RUN_FOREVER);
    set_tacho_command_inx( sn_base[1], TACHO_RUN_FOREVER);
  }
  while(value_gyro>angle){
    get_sensor_value0(sn2, &value_gyro);
    printf("gyro:%f\n",value_gyro);
  }
  set_tacho_command_inx( sn_base[0], TACHO_STOP);
  set_tacho_command_inx( sn_base[1], TACHO_STOP);
  do {
    get_tacho_state_flags( sn_base[0], &stateA );
    get_tacho_state_flags( sn_base[1], &stateD );
  } while ( stateA || stateD );
  //fix angle error
  Sleep(500);
  get_sensor_value0(sn2, &value_gyro);
  printf("gyro:%f\n",value_gyro);
  if(abs(value_gyro)!= abs(angle)+2){
    turn_right_gyro(angle,30);
  }
}

//Turn Right Gyro
//by Adib Rachid
void turn_right_gyro(int angle,int speed){
  set_tacho_speed_sp( sn_base[0], speed);
  set_tacho_speed_sp( sn_base[1], -speed);
  //START

  get_sensor_value0(sn2, &value_gyro);
  if(value_gyro<angle){
    set_tacho_command_inx( sn_base[0], TACHO_RUN_FOREVER);
    set_tacho_command_inx( sn_base[1], TACHO_RUN_FOREVER);
  }
  while(value_gyro<angle){
    get_sensor_value0(sn2, &value_gyro);
    printf("gyro:%f\n",value_gyro);
  }
  set_tacho_command_inx( sn_base[0], TACHO_STOP);
  set_tacho_command_inx( sn_base[1], TACHO_STOP);
  do {
    get_tacho_state_flags( sn_base[0], &stateA );
    get_tacho_state_flags( sn_base[1], &stateD );
  } while ( stateA || stateD );
  //fix angle error
  Sleep(500);
  get_sensor_value0(sn2, &value_gyro);
  printf("gyro:%f\n",value_gyro);
  if(abs(value_gyro)!= abs(angle)){
    turn_left_gyro(angle,30);
  }
}

//chain up
//by Adib Rachid
void chain_up(int value){
  //bottom 0 up 0 -> -335
  if(value<10 || value>335){
    printf("Exceeding limits");
    return;
  }
  set_tacho_speed_sp( snB, 200 );
  get_tacho_position(snB,&position_chain);
  if(position_chain<value){
    set_tacho_command_inx( snB,TACHO_RUN_FOREVER );
  }
  while(position_chain<value){
    get_tacho_position(snB,&position_chain);
  };
  set_tacho_command_inx( snB,TACHO_STOP );
  do {
    get_tacho_state_flags( snB, &stateB );
  } while ( stateB );
}

//chain down
//by Adib Rachid
void chain_down(int value){
  //bottom 0 up 0 -> -345
  if(value<10 || value>335){
    printf("Exceeding limits");
    return;
  }
  set_tacho_speed_sp( snB, -200 );
  get_tacho_position(snB,&position_chain);
  printf("chian valuye %d\n",position_chain);
  if(position_chain>value){
    set_tacho_command_inx( snB,TACHO_RUN_FOREVER );
  }
  while(position_chain>value){
    get_tacho_position(snB,&position_chain);
  };
  set_tacho_command_inx( snB,TACHO_STOP );
  do {
    get_tacho_state_flags( snB, &stateB );
  } while ( stateB );
}

//kicker down
//by Adib Rachid
void kick_prepare(){//180
  set_tacho_speed_sp( snC, -800 );
  get_tacho_position(snC,&position_kick);
  if(position_kick>15){
    set_tacho_command_inx( snC,TACHO_RUN_FOREVER );
  }
  while(position_kick>15){
      get_tacho_position(snC,&position_kick);
  }
  set_tacho_command_inx( snC,TACHO_STOP );
  do {
    get_tacho_state_flags( snC, &stateC );
  } while ( stateC );
}

//kick the ball - shoot
//by Adib Rachid
void kick_action(){//170
  set_tacho_speed_sp( snC, 500 );
  //more efficient if straight 500 if angled 800
  get_tacho_position(snC,&position_kick);
  if(position_kick<160){
    set_tacho_command_inx( snC,TACHO_RUN_FOREVER );
  }
  while(position_kick<160){
      get_tacho_position(snC,&position_kick);
  }
  set_tacho_command_inx( snC,TACHO_STOP );
  do {
    get_tacho_state_flags( snC, &stateC );
  } while ( stateC );
}

//sensors initialisation
//by Adib Rachid
void sensors_init_task(){
  int val,i;
  uint32_t n, ii;
  printf( "Found sensors:\n" );
    for ( i = 0; i < DESC_LIMIT; i++ ) {
      if ( ev3_sensor[ i ].type_inx != SENSOR_TYPE__NONE_ ) {
        printf( "  type = %s\n", ev3_sensor_type( ev3_sensor[ i ].type_inx ));
        printf( "  port = %s\n", ev3_sensor_port_name( i, s ));
        if ( get_sensor_mode( i, s, sizeof( s ))) {
          printf( "  mode = %s\n", s );
        }
        if ( get_sensor_num_values( i, &n )) {
          for ( ii = 0; ii < n; ii++ ) {
            if ( get_sensor_value( ii, i, &val )) {
              printf( "  value%d = %d\n", ii, val );
            }
          }
        }
      }
    }
    if ( ev3_search_sensor( LEGO_EV3_GYRO, &sn2, 0 )) {
      set_sensor_mode_inx(sn2,LEGO_EV3_GYRO_GYRO_RATE);
      set_sensor_mode_inx(sn2,LEGO_EV3_GYRO_GYRO_ANG);
      printf( "GYRO sensor is found,...\n" );
      get_sensor_value1(sn2, &value_gyro);
      printf("gyro: %f\n",value_gyro);
      Sleep(500);
    }else{
      printf("PANIC! GYRO NOT FOUND!");
    }
    if ( ev3_search_sensor( LEGO_EV3_COLOR, &sn4, 0 )) {
      printf( "COLOR sensor is found, reading COLOR...\n" );
      if ( !get_sensor_value( 0, sn4, &value_color ) || ( value_color < 0 ) || ( value_color >= COLOR_COUNT )) {
        value_color = 0;
      }
      printf( "\r(%s) \n", color[ value_color ]);
      fflush( stdout );
    }else{
      printf("PANIC! COLOR NOT FOUND!");
    }
    
    if (ev3_search_sensor(LEGO_EV3_US, &sn3,0)){
      printf("SONAR found, reading sonar...\n");
      if ( !get_sensor_value0(sn3, &value_ultrasonic )) {
        value_ultrasonic = 0;
      }
      printf( "\r(%f) \n", value_ultrasonic);
      fflush( stdout );
    }else{
      printf("PANIC! SONAR NOT FOUND!");
    }
}

//Motor Initialisation
//by Adib Rachid
void motors_init_task(){
  int i;
  for ( i = 0; i < DESC_LIMIT; i++ ) {
    if ( ev3_tacho[ i ].type_inx != TACHO_TYPE__NONE_ ) {
      printf( "  type = %s\n", ev3_tacho_type( ev3_tacho[ i ].type_inx ));
      printf( "  port = %s\n", ev3_tacho_port_name( i, s ));
      printf("  port = %d %d\n", ev3_tacho_desc_port(i), ev3_tacho_desc_extport(i));
    }
  }
    if (!ev3_search_tacho_plugged_in(portA,0, &sn_base[0], 0 ) || !ev3_search_tacho_plugged_in(portD,0, &sn_base[1], 0 )||!ev3_search_tacho_plugged_in(portC,0, &snC, 0 )||!ev3_search_tacho_plugged_in(portB,0, &snB, 0 ) ){    
       printf( "LEGO_EV3_M_MOTOR %d or %d is NOT found\n", portA,portD );
       exit(0);
    }      
    printf("Reseting motors\n");
    set_tacho_position(sn_base[1],0);
    set_tacho_position(sn_base[0],0);
    set_tacho_position(snB,0);
    set_tacho_position(snC,0);
    set_tacho_polarity_inx(snB,TACHO_INVERSED);
    set_tacho_polarity_inx(snC,TACHO_INVERSED);
}

//by Adib, Sanjeev
//identification_combined
void identify(){

  float old_gyro;
  float left_ultrasonic,right_ultrasonic,middle_ultrasonic;
  int a=0;
  //chain up
  chain_up(200);
  //identify up
  Sleep(300);
  get_sensor_value0(sn3, &middle_ultrasonic);
  printf("middle ultrasonic:%f\n",middle_ultrasonic);
  //sleep(200);
  get_sensor_value0(sn2, &value_gyro);
  old_gyro=value_gyro;
  printf("gyro:%f\n",old_gyro);
  turn_left_gyro(old_gyro-25,100);
  get_sensor_value0(sn3, &left_ultrasonic);
  printf("left ultrasonic:%f\n",left_ultrasonic);
  Sleep(150);
  turn_right_gyro(old_gyro+25,100);
  get_sensor_value0(sn3, &right_ultrasonic);
  printf("right ultrasonic:%f\n",right_ultrasonic);
  Sleep(150);
  turn_left_gyro(old_gyro,100);
  Sleep(150);
  printf("R:  %f L: %f  M:  %f",right_ultrasonic,left_ultrasonic,middle_ultrasonic);
  //check more for cylinder
  //add more turnings using gyros
  if((right_ultrasonic < 170 || left_ultrasonic < 170) && middle_ultrasonic<170 ){
    printf("Cube\n");
    return;
  }else if(middle_ultrasonic<170){
    a=1;
    //printf("Cylinder\n");//check if pyramid
    return;
  } 

  //chain down
  chain_down(10);
  //identify down

  get_sensor_value0(sn3, &middle_ultrasonic);
  printf("middle ultrasonic:%f\n",middle_ultrasonic);
  get_sensor_value0(sn2, &value_gyro);
  old_gyro=value_gyro;
  printf("gyro:%f\n",old_gyro);
  turn_left_gyro(old_gyro-25,100);
  get_sensor_value0(sn3, &left_ultrasonic);
  printf("left ultrasonic:%f\n",left_ultrasonic);
  Sleep(150);
  turn_right_gyro(old_gyro+25,100);
  get_sensor_value0(sn3, &right_ultrasonic);
  printf("right ultrasonic:%f\n",right_ultrasonic);
  Sleep(150);
  turn_left_gyro(old_gyro,100);
  Sleep(150);
  printf("R:  %f L: %f  M:  %f",right_ultrasonic,left_ultrasonic,middle_ultrasonic);
  if((right_ultrasonic < 170 || left_ultrasonic < 170) && middle_ultrasonic<170 ){
    if(a==1){
      printf("Pyramid Complete\n");
    }else{
      printf("Cylinder\n");
    }
    return;
  }else{
    //add condition for ball
    printf("Inversed half pyramid\n");//TO FIX
    return;
  } 
  object_identified=1;

}

//identification of the object 
//by Adib Rachid - Sanjeev - Prince
//identify chain up cube cylinder
void identify_object_up(){
  float old_gyro;
  float left_ultrasonic,right_ultrasonic,middle_ultrasonic;
  get_sensor_value0(sn3, &middle_ultrasonic);
  printf("middle ultrasonic:%f\n",middle_ultrasonic);
  get_sensor_value0(sn2, &value_gyro);
  old_gyro=value_gyro;
  printf("gyro:%f\n",old_gyro);
  turn_left_gyro(old_gyro-25,100);
  get_sensor_value0(sn3, &left_ultrasonic);
  printf("left ultrasonic:%f\n",left_ultrasonic);
  Sleep(300);
  turn_right_gyro(old_gyro+25,100);
  get_sensor_value0(sn3, &right_ultrasonic);
  printf("right ultrasonic:%f\n",right_ultrasonic);
  Sleep(300);
  turn_left_gyro(old_gyro,100);
  Sleep(300);
  if((right_ultrasonic < 170 || left_ultrasonic < 170) && middle_ultrasonic<170 ){
    printf("Cube\n");
  }else{
    printf("Cylinder\n");
  } 
  object_identified=1;
}

//Go to specific location defined by x and y
//by Adib Rachid
void go_to_location(float x,float y){
  //get x location of robot
  float x_= loc_x;
  //get y location of robot
  float y_= loc_y;
  int a=1;
  later on add while if find obsctables go around turn left righ ... until arrive to predefined location
  if(x_<x){//should go to + x
    turn_right_gyro(90,200);
  }else{
    turn_left_gyro(-90,200);
    a=-1;
  }
  
  forward(true,abs(x),200);

  turn_left_gyro(0,200);
  if(a){
    if(y_<y){
      turn_left_gyro(0,200);
    }else{
      turn_right_gyro(180,200);
    }   
  }else{
    if(y_<y){
      turn_right_gyro(0,200);
    }else{
      turn_left_gyro(-180,200);
    }     
  }

    forward(true,abs(y),200);
}

//identification of the object bottom pyramid
//by Adib Rachid - Sanjeev - Prince
//identify chain down
void identify_object_middle(){
  float old_gyro;
  float left_ultrasonic,right_ultrasonic,middle_ultrasonic;
  get_sensor_value0(sn3, &middle_ultrasonic);
  printf("middle ultrasonic:%f\n",middle_ultrasonic);
  get_sensor_value0(sn2, &value_gyro);
  old_gyro=value_gyro;
  printf("gyro:%f\n",old_gyro);
  turn_left_gyro(old_gyro-25,100);
  get_sensor_value0(sn3, &left_ultrasonic);
  printf("left ultrasonic:%f\n",left_ultrasonic);
  Sleep(300);
  turn_right_gyro(old_gyro+25,100);
  get_sensor_value0(sn3, &right_ultrasonic);
  printf("right ultrasonic:%f\n",right_ultrasonic);
  Sleep(300);
  turn_left_gyro(old_gyro,100);
  Sleep(300);
  if((right_ultrasonic < 170 || left_ultrasonic < 170) && middle_ultrasonic<170 ){
    printf("HAT\n");//TO FIX
  }else{
    printf("Pyramid Bottom\n");
  } 
  object_identified=1;
}


void turn_until_find(){
  set_tacho_stop_action_inx( sn_base[0], TACHO_COAST );
  set_tacho_speed_sp( sn_base[0], -50);
  //motor D config
  set_tacho_stop_action_inx( sn_base[1], TACHO_COAST );
  set_tacho_speed_sp( sn_base[1], 50);
  //START
  multi_set_tacho_command_inx(sn_base, TACHO_RUN_FOREVER);
  get_sensor_value0(sn3, &value_ultrasonic);
  while(value_ultrasonic>100){
    get_sensor_value0(sn3, &value_ultrasonic);
    printf("ultrasonic:%f\n",value_ultrasonic);
  }
  multi_set_tacho_command_inx(sn_base, TACHO_STOP);
  chain_down(10);
  identify();
  if(object_identified==1){
    chain_up(150);
    forward(true,11,200);
    kick_action();
    Sleep(200);
    kick_prepare();
  }
  //}
}
//Found an object
//by Adib Rachid
void * find(void * varpg){
  // set_tacho_stop_action_inx( sn_base[0], TACHO_COAST );
  // set_tacho_speed_sp( sn_base[0], 200);
  // //motor D config
  // set_tacho_stop_action_inx( sn_base[1], TACHO_COAST );
  // set_tacho_speed_sp( sn_base[1], 200);
  //START
  // multi_set_tacho_command_inx(sn_base, TACHO_RUN_FOREVER);
  // get_sensor_value0(sn3, &value_ultrasonic);
  while(1){
    while(value_ultrasonic>100){
      get_sensor_value0(sn3, &value_ultrasonic);
      printf("ultrasonic:%f\n",value_ultrasonic);
    }
    multi_set_tacho_command_inx(sn_base, TACHO_STOP);
    found_obstacle=true;//set to false after finishing anylysing
    chain_down(10);//from here could be separated
    //should not always identify and save to local map may be matrix
    //if obsctale in memory send to server and do not analyse again!
    identify();
    //if ball found 
    if(object_identified==1){
      chain_up(150);
      forward(true,11,200);
      kick_action();
      Sleep(200);
      kick_prepare();
    }
  }
}

#define PI 3.14159265
//Location Update which shuold be a thread
//By Adib, Prince, Sanjeev
void * update_location(void * vargp){
  int position_A,position_D;
  float value_gyro_t;
  while(1){
    get_tacho_position(sn_base[0],&position_A);
    get_tacho_position(sn_base[1],&position_D);
    new_path=((old_A+position_A)+(old_D+position_D))/2;//- bcz moving opposite
    //printf("A: %d, D: %d, old: %f\n",position_A,position_D,new_path);
    get_sensor_value0(sn2, &value_gyro_t);
    loc_x=loc_x+(sin(value_gyro_t*PI / 180)*new_path)/22.93;
    loc_y=loc_y+(cos(value_gyro_t*PI / 180)*new_path)/22.93;
    //printf("X= %f -  y= %f   - teta= %f  - new_path= %f \n",loc_x,loc_y,value_gyro_t,new_path);
    old_D=-position_D;
    old_A=-position_A;
  }
}

//Start location update thread
// By Adib & Prince
void start_location_updating_thread(){
    printf("Before Thread\n"); 
    pthread_create(&thread_location_id, NULL, update_location, NULL); 
    //pthread_join(thread_location_id, NULL); 
    printf("After Thread\n"); 
}

//start finding obsctacles thread
//By Adib Rachid
void start_find_obsctales(){
    pthread_create(&thread_find_object, NULL, find, NULL); 
    //pthread_join(thread_location_id, NULL); //use this to wait until done or global variables
}



int main( void )
{
  //struct s_move_args *move_args=malloc(sizeof(struct s_move_args));
  //pthread_t threads[10];
//#ifndef __ARM_ARCH_4T__
  /* Disable auto-detection of the brick (you have to set the correct address below) */
  /*ev3_brick_addr = "192.168.0.204";
#endif
  if ( ev3_init() == -1 ) return ( 1 );
#ifndef __ARM_ARCH_4T__
  printf( "The EV3 brick auto-detection is DISABLED,\nwaiting %s online with plugged tacho...\n", ev3_brick_addr );
#else
  printf( "Waiting tacho is plugged...\n" );
#endif*/

  while ( ev3_tacho_init() < 1 ) Sleep( 1000 );
  printf( "*** ( EV3 ) Hello! ***\n" );
  //Motor INITIALISATION
  motors_init_task();
  //SENSOR INITIALISATION
  ev3_sensor_init();
  sensors_init_task();
  start_location_updating_thread();
  start_find_obsctales();
  
  //kick_action();
  //turn_until_find();
  find();
    fflush(stdout);

  //kick_action();
  // Sleep(300);
  // kick_prepare();
  //identify();
//float value_ultra_tmp;
//int val;
// while(1){
//     get_sensor_value0(sn3, &value_ultra_tmp);
//     printf("ultrasonic: %f\n",value_ultra_tmp);
//     //if ( !get_sensor_value( 0, sn4, &val ) || ( val < 0 ) || ( val >= COLOR_COUNT )) {
//     //  val = 0;
//     //}
//     //printf( "Color: %d , %s\n",val, color[ val ]);
//     //fflush( stdout );
//     //Sleep(500);
// }
// while(1);
// turn_right_gyro(90,200);
// Sleep(1000);
// turn_left_gyro(-90,200);
//int position,position2;
//position2=0;
/*while(1){
  get_tacho_position(snB,&position);
  get_tacho_position(snC,&position2);
  printf("position initially:Chain: %d and Base: %d\n",position,position2);
}*/

  ev3_uninit();
  printf( "*** ( EV3 ) Bye! ***\n" );

  return ( 0 );
  while(1);
}


/* CODE MODIFIED AND NOT USED ANYMORE - MAY USE LATER ON
//identification of the object small triangle
//by Adib Rachid 
//small trianggle
/*void identify_object_small_pyramid(){
  float old_gyro;
  float left_ultrasonic,right_ultrasonic,middle_ultrasonic;
  get_sensor_value0(sn3, &middle_ultrasonic);
  printf("middle ultrasonic:%f\n",middle_ultrasonic);
  get_sensor_value0(sn2, &value_gyro);
  old_gyro=value_gyro;
  printf("gyro:%f\n",old_gyro);
  turn_left_gyro(old_gyro-25,100);
  get_sensor_value0(sn3, &left_ultrasonic);
  printf("left ultrasonic:%f\n",left_ultrasonic);
  Sleep(300);
  turn_right_gyro(old_gyro+25,100);
  get_sensor_value0(sn3, &right_ultrasonic);
  printf("right ultrasonic:%f\n",right_ultrasonic);
  Sleep(300);
  turn_left_gyro(old_gyro,100);
  Sleep(300);
  if((right_ultrasonic < 170 || left_ultrasonic < 170) && middle_ultrasonic<170 ){
    printf("HAT\n");//TO FIX
  }else{
    printf("Pyramid Bottom\n");
  } 
  object_identified=1;
}*/
// void * forward_p(void * tmp){
//   struct s_move_args * args= (struct s_move_args *) tmp;
//   move(tmp->dist_not_time,tmp->value,tmp->value,tmp->speed,tmp->speed );
//   free(tmp);
// }

/*static bool _check_pressed( uint8_t sn )
{
  int val;

  if ( sn == SENSOR__NONE_ ) {
    return ( ev3_read_keys(( uint8_t *) &val ) && ( val & EV3_KEY_UP ));
  }
  return ( get_sensor_value( 0, sn, &val ) && ( val != 0 ));
}*/

//struct for move thread arguments
//by Adib Rachid
// typedef struct s_move_args {
//     bool dist_not_time;
//     int value;
//     int speed;
// };

// //fill the move arguments structure
// //by Adib Rachid
// s_move_args fill_move_arguments(s_move_args tmp, bool d, int v,int s){
//   tmp->dist_not_time=d;
//   tmp->value=v;
//   tmp->speed=s;
//   return tmp;
// }

//Chain Down old
//by Adib Rachid
/*void chain_down(int value){
  //bottom 0 up 0 -> -357
  //motor A config
  if(chain_pos-value<0){
    printf("Can't go Lower");
    return; 
  }
  chain_pos-=value;
  set_tacho_speed_sp( snB, 100 );
  //set_tacho_ramp_up_sp( snB, 1000 );
  set_tacho_ramp_down_sp( snB, 1000 );
  set_tacho_position_sp( snB, value );
  set_tacho_command_inx( snB, TACHO_RUN_TO_REL_POS );  
  printf("chain donw\n");
  do {
    get_tacho_state_flags( snB, &stateB );
  } while ( stateB );
}*/

//kicker down based on : go to this value
//by Adib Rachid
/*void kick_prepare_old(){//180
  set_tacho_speed_sp( snC, 500 );
  set_tacho_position_sp( snC,180  );
  set_tacho_command_inx( snC, TACHO_RUN_TO_REL_POS );
  do {
    get_tacho_state_flags( snC, &stateC );
  } while ( stateC );
}*/

//kick the ball - shoot
//by Adib Rachid
/*void kick_action_old(){
  set_tacho_speed_sp( snC, -500 );
  set_tacho_position_sp( snC,-180  );
  set_tacho_command_inx( snC, TACHO_RUN_TO_REL_POS );
  do {
    get_tacho_state_flags( snC, &stateC );
  } while ( stateC );
}*/

//chain up old
//by Adib Rachid
/*void chain_up_old(int value){
  //bottom 0 up 0 -> -357
  //motor A config
  if(chain_pos+value>350){
    printf("Can't go higher");
    return; 
  }
  chain_pos+=value;
  set_tacho_speed_sp( snB, -200 );
  //set_tacho_ramp_up_sp( snB, 1000 );
  //set_tacho_ramp_down_sp( snB, 1000 );
  set_tacho_position_sp( snB, -value );
  set_tacho_command_inx( snB, TACHO_RUN_TO_REL_POS );  
  printf("HI\n" );
  do {
    get_tacho_state_flags( snB, &stateB );
  } while ( stateB );
}*/

//to implement:
//Location update Thread: implement thread calculating location always using encoder -  Sanjeev - JF
//Obstacles: thread running, when find an object, stop and try to identify it - Adib
//fix first rotation at 1st sec when starting to move- maybe raise ramping value - ALL
//Threads: implement and test threads - Prince
//Implement main function - Prince
//website
/*

Test #1. Be able to find an object in your side (without identifying its shape). For this test, only one object is put in your side.
Test #2. Be able to identify an object positioned in your side. For this test, only one object is placed in your side.
Test #3. Be able to identify an object and estimate its position.
Test #4. Be able to find a ball located at a predefined position.
DONE  Test #5. Be able to shoot a ball in the opposite side. You can place your robot and the ball as you wish.
LATER Test #6. Be able to connect to the server and inform the server when an object was detected.
*/