#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include <stdarg.h>
#include <time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdint.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
#include "dummyserver.h"
// WIN32 /////////////////////////////////////////
#ifdef __WIN32__

#include <windows.h>

// UNIX //////////////////////////////////////////
#else

#include <unistd.h>

#define Sleep(msec) usleep((msec)*1000)

//////////////////////////////////////////////////
#endif
const char const *color[] = {"?", "BLACK", "BLUE", "GREEN", "YELLOW", "RED", "WHITE", "BROWN"};
#define COLOR_COUNT ((int)(sizeof(color) / sizeof(color[0])))
//initialisation
int portA = OUTPUT_A;
float dist_to_obj_angle;
int portD = OUTPUT_D;
int portB = OUTPUT_B;
int portC = OUTPUT_C;
pthread_mutex_t lock;
int chain_pos = 0;
float value_gyro;
float value_ultrasonic;
int value_color;
uint8_t sn;
char s[256];
//uint8_t sn_base[0];//Left
//uint8_t sn_base[1];//right
uint8_t snB; //chain
uint8_t snC; //kick
uint8_t sn_base[2];
uint8_t sn1;
uint8_t sn2; //gyro
uint8_t sn3; //UltraSonic
uint8_t sn4; //ColorSensor
FLAGS_T state;
FLAGS_T stateA;
FLAGS_T stateD;
FLAGS_T stateB;
FLAGS_T stateC;
int object_identified; //0:ball  1:cube  2:triangle up  3:cylindre  4:triangle down
int position_kick;
int position_chain;
float new_path = 0;
float old_D = 0;
float old_A = 0;
float loc_y_update = 0;
float loc_x_update = 0;
float loc_y = 25; //initial
float loc_x = 50; //location for x and y
pthread_t thread_location_id;
pthread_t thread_find_object;

pthread_mutex_t lockOrientation;
pthread_mutex_t lockObjectFound;
pthread_mutex_t lockMsg;
pthread_t send_thread;
pthread_t connect_thread;
struct arg_msg_struct
{
    int arg_sockfd;
    int arg_msg_type;
    int arg_obj_type;
    int arg_x;
    int arg_y;
    int connect_type;
} msg_args;

float angle_obj_detected;

int sockfd, connfd;
int serv;
uint16_t msgId = 0;

//combined move function
//by Adib Rachid
void move(bool dist_not_time, int valueA, int valueD, int speedA, int speedD)
{
    printf("forward begin");
    //Run motors in order from port A to D
    if (!dist_not_time)
    {
        //motor A config
        set_tacho_stop_action_inx(sn_base[0], TACHO_COAST);
        set_tacho_speed_sp(sn_base[0], speedA);
        set_tacho_time_sp(sn_base[0], valueA);
        set_tacho_ramp_up_sp(sn_base[0], 2000);
        set_tacho_ramp_down_sp(sn_base[0], 2000);
        //motor D config
        set_tacho_stop_action_inx(sn_base[1], TACHO_COAST);
        set_tacho_speed_sp(sn_base[1], speedD);
        set_tacho_time_sp(sn_base[1], valueD);
        set_tacho_ramp_up_sp(sn_base[1], 2000);
        set_tacho_ramp_down_sp(sn_base[1], 2000);
        //START
        if (valueA * valueD >= 0)
        {
            multi_set_tacho_command_inx(sn_base, TACHO_RUN_TIMED);
        }
        else
        {
            set_tacho_command_inx(sn_base[0], TACHO_RUN_TIMED);
            set_tacho_command_inx(sn_base[1], TACHO_RUN_TIMED);
        }
    }
    else
    {
        //diameter of the wheel: 5cm -> R=4.5cm -> perimeter = 2* pi* R = 15.7  so 360 unite -> 15.7cm => 1cm=> 22.93
        int error = 0;
        error = ceil(valueA / 150); //error after testing many times on diff. distances and speed values
        if (speedA > 0)
            valueA = valueA * 22.95 - error;
        else
            valueA = valueA * 22.95 + error;
        if (speedD > 0)
            valueD = valueD * 22.95 - error;
        else
            valueD = valueD * 22.95 + error;
        //motor A config
        set_tacho_speed_sp(sn_base[0], speedA);
        set_tacho_ramp_up_sp(sn_base[0], 3000);
        set_tacho_ramp_down_sp(sn_base[0], 3000);
        set_tacho_position_sp(sn_base[0], valueA);
        //motor B config
        set_tacho_speed_sp(sn_base[1], speedD);
        set_tacho_ramp_up_sp(sn_base[1], 3000);
        set_tacho_ramp_down_sp(sn_base[1], 3000);
        set_tacho_position_sp(sn_base[1], valueD);
        //START
        if (valueA * valueD >= 0)
        {
            multi_set_tacho_command_inx(sn_base, TACHO_RUN_TO_REL_POS);
        }
        else
        {
            set_tacho_command_inx(sn_base[0], TACHO_RUN_TO_REL_POS);
            set_tacho_command_inx(sn_base[1], TACHO_RUN_TO_REL_POS);
        }
    }

    /* Wait tacho stop */
    //Sleep( 1 );
    do
    {
        get_tacho_state_flags(sn_base[0], &stateA);
        get_tacho_state_flags(sn_base[1], &stateD);
    } while (stateA || stateD);
    printf("forward done");
}

float MIN(float x, float y)
{
    if (x < y)
    {
        return x;
    }
    return y;
}
//Move Backward
//by Adib Rachid
void backward(bool dist_not_time, int value, int speed)
{
    move(dist_not_time, -value, -value, -speed, -speed);
}

//Move Forward
//by Adib Rachid
void forward(bool dist_not_time, int value, int speed)
{
    move(dist_not_time, value, value, speed, speed);
}

//Turn Right
//by Adib Rachid
void turn_right(bool dist_not_time, int value, int speed)
{
    move(dist_not_time, -value, value, -speed, speed);
}

//Turn Left
//by Adib Rachid
void turn_left(bool dist_not_time, int value, int speed)
{
    move(dist_not_time, value, -value, speed, -speed);
}

//prince
void get_compass_value(float *value)
{
    float currentOrientation;
    get_sensor_value0(sn2, &currentOrientation);
    if (currentOrientation >= 0)
    {
        *value = (int)currentOrientation % 360;
    }
    else
    {
        *value = (((int)currentOrientation % -360) + 360) % 360;
    }
}

//Turn Left Gyro -> left gyro value decreases
//by Adib Rachid
void turn_left_gyro(int angle, int speed)
{
    set_tacho_speed_sp(sn_base[0], -speed);
    set_tacho_speed_sp(sn_base[1], speed);
    //START

    get_sensor_value0(sn2, &value_gyro);
    if (value_gyro > angle)
    {
        set_tacho_command_inx(sn_base[0], TACHO_RUN_FOREVER);
        set_tacho_command_inx(sn_base[1], TACHO_RUN_FOREVER);
    }
    while (value_gyro > angle)
    {
        get_sensor_value0(sn2, &value_gyro);
        ////printf("gyro:%f\n", value_gyro);
    }
    set_tacho_command_inx(sn_base[0], TACHO_STOP);
    set_tacho_command_inx(sn_base[1], TACHO_STOP);
    // do
    // {
    //     get_tacho_state_flags(sn_base[0], &stateA);
    //     get_tacho_state_flags(sn_base[1], &stateD);
    // } while (stateA || stateD);
    //fix angle error
    Sleep(500);
    get_sensor_value0(sn2, &value_gyro);
    //printf("gyro:%f\n", value_gyro);
    if ((abs(value_gyro) != abs(angle)))
    {
        printf("turn_left_gyro: lock\n");
        //c += 1;
        turn_right_gyro(angle, 15);
    }
}

//Turn Right Gyro
//by Adib Rachid
void turn_right_gyro(int angle, int speed)
{
    set_tacho_speed_sp(sn_base[0], speed);
    set_tacho_speed_sp(sn_base[1], -speed);
    //START

    get_sensor_value0(sn2, &value_gyro);
    if (value_gyro < angle)
    {
        set_tacho_command_inx(sn_base[0], TACHO_RUN_FOREVER);
        set_tacho_command_inx(sn_base[1], TACHO_RUN_FOREVER);
    }
    while (value_gyro < angle)
    {
        get_sensor_value0(sn2, &value_gyro);
        //printf("gyro:%f\n", value_gyro);
    }
    set_tacho_command_inx(sn_base[0], TACHO_STOP);
    set_tacho_command_inx(sn_base[1], TACHO_STOP);
    // do
    // {
    //     get_tacho_state_flags(sn_base[0], &stateA);
    //     get_tacho_state_flags(sn_base[1], &stateD);
    // } while (stateA || stateD);
    //fix angle error
    Sleep(500);
    get_sensor_value0(sn2, &value_gyro);
    printf("gyro    :%f\n", value_gyro);
    if ((abs(value_gyro) != abs(angle)))
    {
        printf("turn_left_gyro: lock\n");

        turn_left_gyro(angle, 15);
    }
}

//Go to specific location defined by x and y
//by Adib Rachid

/*
void go_to_location(float x, float y)
{
    //get x location of robot

    //pthread_mutex_lock(&lock);
    float x_ = loc_x_update;
    x = x - 50;
    y = y - 25;
    //get y location of robot
    float y_ = loc_y_update;
    //pthread_mutex_unlock(&lock);
    int a = 1;
    //later on add while if find obsctables go around turn left righ ... until arrive to predefined location
    if (x_ < x)
    { //should go to + x
        printf("1");
        turn_right_gyro(90, 200);
        printf("2");
    }
    else
    {
        printf("3");

        turn_left_gyro(-90, 200);
        printf("4");
        a = -1;
    }
    printf("hi");
    forward(true, abs(x), 200);
    //loc_x += x;
    printf("by");
    //turn_left_gyro(0, 200);
    if (a)
    {
        if (y_ < y)
        {
            printf("5");
            turn_left_gyro(0, 200);
            printf("6");
        }
        else
        {
            printf("7");
            turn_right_gyro(180, 200);
            printf("8");
        }
    }
    else
    {
        if (y_ < y)
        {
            printf("9");
            turn_right_gyro(0, 200);
        }
        else
        {
            printf("10");
            turn_left_gyro(-180, 200);
        }
    }
    printf("hi1");
    forward(true, abs(y), 200);
    //loc_x += x;
    //loc_y += y;
    printf("by1");
    //loc_y += y;
    printf("finishedfinishedfinishedfinishedfinishedfinishedfinishedfinished");
}
*/
void go_to_location_dummy(float x, float y)
{
    //get_tacho_position(snC, &position_kick);
    //printf("kick position %d", position_kick);
    forward(true, 20, 200);
    kick_action();
    Sleep(200);
    kick_prepare();
    //turn_left_gyro(-90, 200);
    //forward(true, 50, 200);
}
void go_to_location(float x, float y)
{
    //get x location of robot
    //x = x - 50;
    //y = y - 25;
    float x_ = loc_x;
    //get y location of robot
    float y_ = loc_y;
    int a = 1;

    //later on add while if find obsctables go around turn left righ... until arrive to predefined location
    if (x_ < x)
    { //should go to + x
        turn_right_gyro(90, 200);
    }
    else
    {
        turn_left_gyro(-90, 200);
        a = -1;
    }

    if (x != x_)
    {
        /*
        float r=x-x_;
        float a=r/10;
        while(a<10){
            forward(true, abs(a), 200);
            a+=r/10;
            if(sensor<10){
                identify()
            }    
        }
        */
        printf("x notx_ x_=%d and x=%d\n", x_, x);
        forward(true, abs(x - x_), 200);
    }
    turn_left_gyro(0, 200);
    if (a)
    {
        if (y_ < y)
        {
            turn_left_gyro(0, 200);
        }
        else
        {
            turn_right_gyro(180, 200);
        }
    }
    else
    {
        if (y_ < y)
        {
            turn_right_gyro(0, 200);
        }
        else
        {
            turn_left_gyro(-180, 200);
        }
    }
    //printf("x:%f and x_:%f\n",x,x_);
    if (y != y_)
    {
        forward(true, abs(y - y_), 200);
    }
    //printf("y:%f and y_:%f\n",x,x_);
    loc_x = x;
    //printf("locx=:%f\n",loc_x);
    loc_y = y;
    //printf("locy=:%f\n",loc_y);
}

//chain up
//by Adib Rachid
void chain_up(int value)
{
    //bottom 0 up 0 -> -335
    if (value < 10 || value > 335)
    {
        printf("Exceeding limits");
        return;
    }
    set_tacho_speed_sp(snB, 200);
    get_tacho_position(snB, &position_chain);
    if (position_chain < value)
    {
        set_tacho_command_inx(snB, TACHO_RUN_FOREVER);
    }
    while (position_chain < value)
    {
        get_tacho_position(snB, &position_chain);
    };
    set_tacho_command_inx(snB, TACHO_STOP);
    do
    {
        get_tacho_state_flags(snB, &stateB);
    } while (stateB);
}

//chain down
//by Adib Rachid
void chain_down(int value)
{
    //bottom 0 up 0 -> -345
    if (value < 10 || value > 335)
    {
        printf("Exceeding limits");
        return;
    }
    set_tacho_speed_sp(snB, -200);
    get_tacho_position(snB, &position_chain);
    printf("chian valuye %d\n", position_chain);
    if (position_chain > value)
    {
        set_tacho_command_inx(snB, TACHO_RUN_FOREVER);
    }
    while (position_chain > value)
    {
        get_tacho_position(snB, &position_chain);
    };
    set_tacho_command_inx(snB, TACHO_STOP);
    do
    {
        get_tacho_state_flags(snB, &stateB);
    } while (stateB);
}

//prince
void chain_settle()
{
    set_tacho_speed_sp(snB, -200);
    get_tacho_position(snB, &position_chain);
    printf("chain value %d\n", position_chain);
    if (position_chain > 10)
    {
        chain_down(10);
    }
}

//kicker down
//by Adib Rachid
void kick_prepare()
{ //180
    set_tacho_speed_sp(snC, -800);
    get_tacho_position(snC, &position_kick);
    if (position_kick > 15)
    {
        set_tacho_command_inx(snC, TACHO_RUN_FOREVER);
    }
    while (position_kick > 15)
    {
        get_tacho_position(snC, &position_kick);
    }
    set_tacho_command_inx(snC, TACHO_STOP);
    do
    {
        get_tacho_state_flags(snC, &stateC);
    } while (stateC);
}

//kick the ball - shoot
//by Adib Rachid
void kick_action()
{ //170
    set_tacho_speed_sp(snC, 1050);
    get_tacho_position(snC, &position_kick);
    if (position_kick < 160)
    {
        set_tacho_command_inx(snC, TACHO_RUN_FOREVER);
    }
    while (position_kick < 160)
    {
        get_tacho_position(snC, &position_kick);
    }
    set_tacho_command_inx(snC, TACHO_STOP);
    do
    {
        get_tacho_state_flags(snC, &stateC);
    } while (stateC);
}

//sensors initialisation
//by Adib Rachid
void sensors_init_task()
{
    int val, i;
    uint32_t n, ii;
    printf("Found sensors:\n");
    for (i = 0; i < DESC_LIMIT; i++)
    {
        if (ev3_sensor[i].type_inx != SENSOR_TYPE__NONE_)
        {
            printf("  type = %s\n", ev3_sensor_type(ev3_sensor[i].type_inx));
            printf("  port = %s\n", ev3_sensor_port_name(i, s));
            if (get_sensor_mode(i, s, sizeof(s)))
            {
                printf("  mode = %s\n", s);
            }
            if (get_sensor_num_values(i, &n))
            {
                for (ii = 0; ii < n; ii++)
                {
                    if (get_sensor_value(ii, i, &val))
                    {
                        printf("  value%d = %d\n", ii, val);
                    }
                }
            }
        }
    }
    if (ev3_search_sensor(LEGO_EV3_GYRO, &sn2, 0))
    {
        set_sensor_mode_inx(sn2, LEGO_EV3_GYRO_GYRO_RATE);
        set_sensor_mode_inx(sn2, LEGO_EV3_GYRO_GYRO_ANG);
        printf("GYRO sensor is found,...\n");
        get_sensor_value1(sn2, &value_gyro);
        //printf("gyro: %f\n", value_gyro);
    }
    else
    {
        printf("PANIC! GYRO NOT FOUND!");
    }
    if (ev3_search_sensor(LEGO_EV3_COLOR, &sn4, 0))
    {
        printf("COLOR sensor is found, reading COLOR...\n");
        if (!get_sensor_value(0, sn4, &value_color) || (value_color < 0) || (value_color >= COLOR_COUNT))
        {
            value_color = 0;
        }
        printf("\r(%s) \n", color[value_color]);
        fflush(stdout);
    }
    else
    {
        printf("PANIC! COLOR NOT FOUND!");
    }

    if (ev3_search_sensor(LEGO_EV3_US, &sn3, 0))
    {
        printf("SONAR found, reading sonar...\n");
        if (!get_sensor_value0(sn3, &value_ultrasonic))
        {
            value_ultrasonic = 0;
        }
        printf("\r(%f) \n", value_ultrasonic);
        fflush(stdout);
    }
    else
    {
        printf("PANIC! SONAR NOT FOUND!");
    }
}

//Motor Initialisation
//by Adib Rachid
void motors_init_task()
{
    int i;
    printf("Found tacho motors:\n");
    for (i = 0; i < DESC_LIMIT; i++)
    {
        if (ev3_tacho[i].type_inx != TACHO_TYPE__NONE_)
        {
            printf("  type = %s\n", ev3_tacho_type(ev3_tacho[i].type_inx));
            printf("  port = %s\n", ev3_tacho_port_name(i, s));
            printf("  port = %d %d\n", ev3_tacho_desc_port(i), ev3_tacho_desc_extport(i));
        }
    }
    if (!ev3_search_tacho_plugged_in(portA, 0, &sn_base[0], 0) ||
        !ev3_search_tacho_plugged_in(portD, 0, &sn_base[1], 0) || !ev3_search_tacho_plugged_in(portC, 0, &snC, 0) ||
        !ev3_search_tacho_plugged_in(portB, 0, &snB, 0))
    {
        printf("LEGO_EV3_M_MOTOR %d or %d is NOT found\n", portA, portD);
        exit(0);
    }
    printf("Reseting motors\n");
    set_tacho_position(sn_base[1], 0);
    set_tacho_position(sn_base[0], 0);
    set_tacho_position(snB, 0);
    set_tacho_position(snC, 0);
    set_tacho_polarity_inx(snB, TACHO_INVERSED);
    set_tacho_polarity_inx(snC, TACHO_INVERSED);
}

//identification of the object
//by Adib Rachid
void identify_object()
{
    float old_gyro;
    float left_ultrasonic, right_ultrasonic;
    get_sensor_value0(sn2, &value_gyro);
    old_gyro = value_gyro;
    ////printf("gyro:%f\n", old_gyro);
    turn_left_gyro(old_gyro - 5, 100);
    get_sensor_value0(sn3, &left_ultrasonic);
    printf("left ultrasonic:%f\n", left_ultrasonic);
    Sleep(1000);
    turn_right_gyro(old_gyro + 5, 100);
    get_sensor_value0(sn3, &right_ultrasonic);
    printf("right ultrasonic:%f\n", right_ultrasonic);
    Sleep(1000);
    turn_left_gyro(old_gyro, 100);
    Sleep(500);
    object_identified = 1;
    printf("object identified: %d\n", object_identified);
}

//by Adib, Sanjeev
//identification_combined
void identify()
{

    float old_gyro;
    float left_ultrasonic, right_ultrasonic, middle_ultrasonic;
    int a = 0;
    //chain up
    chain_up(200);
    //identify up
    Sleep(300);
    get_sensor_value0(sn3, &middle_ultrasonic);
    printf("middle ultrasonic:%f\n", middle_ultrasonic);
    //sleep(200);
    get_sensor_value0(sn2, &value_gyro);
    old_gyro = value_gyro;
    printf("gyro:%f\n", old_gyro);
    turn_left_gyro(old_gyro - 25, 100);
    get_sensor_value0(sn3, &left_ultrasonic);
    printf("left ultrasonic:%f\n", left_ultrasonic);
    Sleep(150);
    turn_right_gyro(old_gyro + 25, 100);
    get_sensor_value0(sn3, &right_ultrasonic);
    printf("right ultrasonic:%f\n", right_ultrasonic);
    Sleep(150);
    turn_left_gyro(old_gyro, 100);
    Sleep(150);
    printf("R:  %f L: %f  M:  %f", right_ultrasonic, left_ultrasonic, middle_ultrasonic);
    //check more for cylinder
    //add more turnings using gyros
    if ((right_ultrasonic < 170 || left_ultrasonic < 170) && middle_ultrasonic < 170)
    {
        printf("Cube\n");
        return;
    }
    else if (middle_ultrasonic < 170)
    {
        a = 1;
        //printf("Cylinder\n");//check if pyramid
        return;
    }

    //chain down
    chain_down(10);
    //identify down

    get_sensor_value0(sn3, &middle_ultrasonic);
    printf("middle ultrasonic:%f\n", middle_ultrasonic);
    get_sensor_value0(sn2, &value_gyro);
    old_gyro = value_gyro;
    printf("gyro:%f\n", old_gyro);
    turn_left_gyro(old_gyro - 25, 100);
    get_sensor_value0(sn3, &left_ultrasonic);
    printf("left ultrasonic:%f\n", left_ultrasonic);
    Sleep(150);
    turn_right_gyro(old_gyro + 25, 100);
    get_sensor_value0(sn3, &right_ultrasonic);
    printf("right ultrasonic:%f\n", right_ultrasonic);
    Sleep(150);
    turn_left_gyro(old_gyro, 100);
    Sleep(150);
    printf("R:  %f L: %f  M:  %f", right_ultrasonic, left_ultrasonic, middle_ultrasonic);
    if ((right_ultrasonic < 170 || left_ultrasonic < 170) && middle_ultrasonic < 170)
    {
        if (a == 1)
        {
            printf("Pyramid Complete\n");
        }
        else
        {
            printf("Cylinder\n");
        }
        return;
    }
    else
    {
        //add condition for ball
        printf("Inversed half pyramid\n"); //TO FIX
        return;
    }
    object_identified = 1;
}

/*void identify()
{

    float old_gyro;
    float left_ultrasonic, right_ultrasonic, middle_ultrasonic;
    int a = 0;
    //chain up
    chain_up(200);
    //identify up
    Sleep(300);
    get_sensor_value0(sn3, &middle_ultrasonic);
    printf("middle ultrasonic:%f\n", middle_ultrasonic);
    //sleep(200);
    get_sensor_value0(sn2, &value_gyro);
    old_gyro = value_gyro;
    ////printf("gyro:%f\n", old_gyro);
    turn_left_gyro(old_gyro - 25, 100);
    get_sensor_value0(sn3, &left_ultrasonic);
    printf("left ultrasonic:%f\n", left_ultrasonic);
    Sleep(150);
    turn_right_gyro(old_gyro + 25, 100);
    get_sensor_value0(sn3, &right_ultrasonic);
    printf("right ultrasonic:%f\n", right_ultrasonic);
    Sleep(150);
    turn_left_gyro(old_gyro, 100);
    Sleep(150);
    printf("R:  %f L: %f  M:  %f\n", right_ultrasonic, left_ultrasonic, middle_ultrasonic);
    //check more for cylinder
    //add more turnings using gyros
    if ((right_ultrasonic < 170 || left_ultrasonic < 170) && middle_ultrasonic < 170)
    {
        //0:ball  1:cube  2:triangle up  3:cylindre  4:triangle down
        object_identified = 1;
        printf("Cube\n");
        return;
    }
    else if (middle_ultrasonic < 170)
    {
        a = 1;
        printf("Cylinder\n"); //check if pyramid
        object_identified = 3;
        return;
    }

    //chain down
    chain_down(10);
    //identify down

    get_sensor_value0(sn3, &middle_ultrasonic);
    printf("middle ultrasonic:%f\n", middle_ultrasonic);
    get_sensor_value0(sn2, &value_gyro);
    old_gyro = value_gyro;
    ////printf("gyro:%f\n", old_gyro);
    turn_left_gyro(old_gyro - 25, 100);
    get_sensor_value0(sn3, &left_ultrasonic);
    printf("left ultrasonic:%f\n", left_ultrasonic);
    Sleep(150);
    turn_right_gyro(old_gyro + 25, 100);
    get_sensor_value0(sn3, &right_ultrasonic);
    printf("right ultrasonic:%f\n", right_ultrasonic);
    Sleep(150);
    turn_left_gyro(old_gyro, 100);
    Sleep(150);
    printf("R:  %f L: %f  M:  %f\n", right_ultrasonic, left_ultrasonic, middle_ultrasonic);
    if ((right_ultrasonic < 170 || left_ultrasonic < 170) && middle_ultrasonic < 170)
    {
        if (a == 1)
        {
            printf("Pyramid Complete\n");
        }
        else
        {
            printf("Cylinder\n");
        }
        return;
    }
    else
    {
        object_identified = 4;
        //add condition for ball
        printf("Inversed half pyramid\n"); //TO FIX
        return;
    }
    printf("ball value %d\n", object_identified);
    object_identified = 0;
}*/

//testing forward until finds obstacle then scan
//by Adib Rachid
void forward_until_find()
{
    set_tacho_stop_action_inx(sn_base[0], TACHO_COAST);
    set_tacho_speed_sp(sn_base[0], 200);
    //motor D config
    set_tacho_stop_action_inx(sn_base[1], TACHO_COAST);
    set_tacho_speed_sp(sn_base[1], 200);
    //START
    multi_set_tacho_command_inx(sn_base, TACHO_RUN_FOREVER);
    get_sensor_value0(sn3, &value_ultrasonic);
    while (value_ultrasonic > 100)
    {
        get_sensor_value0(sn3, &value_ultrasonic);
        printf("ultrasonic:%f\n", value_ultrasonic);
    }
    multi_set_tacho_command_inx(sn_base, TACHO_STOP);
    chain_down(10);
    identify_object();
    if (object_identified == 1)
    {
        chain_up(150);
        forward(true, 11, 200);
        kick_action();
        Sleep(200);
        kick_prepare();
    }
}

void turn_until_find()
{
    set_tacho_stop_action_inx(sn_base[0], TACHO_COAST);
    set_tacho_speed_sp(sn_base[0], -100);
    //motor D config
    set_tacho_stop_action_inx(sn_base[1], TACHO_COAST);
    set_tacho_speed_sp(sn_base[1], 100);
    //START
    multi_set_tacho_command_inx(sn_base, TACHO_RUN_FOREVER);
    get_sensor_value0(sn3, &value_ultrasonic);
    while (value_ultrasonic > 300)
    {
        get_sensor_value0(sn3, &value_ultrasonic);
        printf("ultrasonic:%f\n", value_ultrasonic);
    }
    printf("Object found :)");
    multi_set_tacho_command_inx(sn_base, TACHO_STOP);
    /*chain_down(10);
    identify_object();
    if(object_identified==1){
      chain_up(150);
      forward(true,11,200);*/
    //kick_action();
    //Sleep(200);
    //kick_prepare();
    //}
}

#define PI 3.14159265
//Location Update which shuold be a thread
//By Adib, Prince, Sanjeev
void *update_location(void *vargp)
{
    int position_A, position_D;
    float value_gyro_t;
    while (1)
    {
        get_tacho_position(sn_base[0], &position_A);
        get_tacho_position(sn_base[1], &position_D);
        new_path = ((old_A + position_A) + (old_D + position_D)) / 2; //- bcz moving opposite
        //printf("A: %d, D: %d, old: %f\n",position_A,position_D,new_path);
        get_sensor_value0(sn2, &value_gyro_t);
        pthread_mutex_lock(&lock);
        loc_x_update = (loc_x_update + (sin(value_gyro_t * PI / 180) * new_path) / 22.93);
        loc_y_update = (loc_y_update + (cos(value_gyro_t * PI / 180) * new_path) / 22.93);
        loc_x = loc_x_update + 50;
        loc_y = loc_y_update + 25;
        pthread_mutex_unlock(&lock);
        //printf("X= %f -  y= %f   - teta= %f  - new_path= %f \n", loc_x, loc_y, value_gyro_t, new_path);
        old_D = -position_D;
        old_A = -position_A;
    }
}

//Start location update thread
// By Adib & Prince
void start_location_updating_thread()
{
    printf("Before Thread\n");
    pthread_create(&thread_location_id, NULL, update_location, NULL);
    //pthread_join(thread_location_id, NULL);
    printf("After Thread\n");
}
/*
//start finding obsctacles thread
//By Adib Rachid
void start_find_obsctales()
{
    pthread_create(&thread_find_object, NULL, find, NULL);
    //pthread_join(thread_location_id, NULL); //use this to wait until done or global variables
}*/

//JEAN & PRINCE
bool check_if_obj_in_sight()
{
    float value_gyro, value_gyro_new, value_gyro_new_2nd, gyro_start, gyro_end;

    //chain_settle();
    //get_sensor_value0(sn2, &value_gyro);
    get_compass_value(&value_gyro);
    gyro_start = value_gyro;
    value_gyro += 10;
    //We want to turn until nearly getting back to old angle

    set_tacho_stop_action_inx(sn_base[0], TACHO_COAST);
    set_tacho_speed_sp(sn_base[0], -50);
    //motor D config
    set_tacho_stop_action_inx(sn_base[1], TACHO_COAST);
    set_tacho_speed_sp(sn_base[1], 50);
    //START
    multi_set_tacho_command_inx(sn_base, TACHO_RUN_FOREVER);
    get_sensor_value0(sn3, &value_ultrasonic);
    float value_ultrasonic_save;

    bool completeTurn = false;

    while (value_ultrasonic > 150 && !completeTurn)
    {
        get_sensor_value0(sn3, &value_ultrasonic);
        //get_sensor_value0(sn3, &value_ultrasonic_save);
        //get_sensor_value0(sn2, &value_gyro_new);
        get_compass_value(&value_gyro_new);
        completeTurn = abs(value_gyro_new - value_gyro) < 5;
        //completeTurn = value_gyro_new == value_gyro;
        if (completeTurn)
        {
            printf("complete turn\n");
            gyro_end = value_gyro_new;
        }
        printf("gyronew:%f\n", value_gyro_new);
        ////printf("gyro:%f\n", value_gyro);
        printf("ultrasonic:%f\n", value_ultrasonic);
    }
    //second while to detect the angle of the second edge of the object and calculate the middle angle
    while (value_ultrasonic < 150)
    {
        //While we are still seeing the object
        get_sensor_value0(sn3, &value_ultrasonic);
        //get_sensor_value0(sn2, &value_gyro_new_2nd);
        get_compass_value(&value_gyro_new_2nd);
    }
    //if not complete turn we save the angle where is the object

    if (!completeTurn)
    {
        angle_obj_detected = (value_gyro_new + value_gyro_new_2nd) / 2;
        printf("angle object: %f\n", angle_obj_detected);
    }
    multi_set_tacho_command_inx(sn_base, TACHO_STOP);

    //We return to initial angle
    //17_01_20 : try to correct bug =================================================
    //Commented :    turn_right_gyro(value_gyro - 10, 100);
    //rest_gyro(10, 100);

    printf("gyrostart:%f\n", gyro_start);
    printf("gyroend:%f\n", gyro_end);

    if (completeTurn)
    {
        //17_01_20 : try to correct bug =================================================
        turn_right_gyro(value_gyro - 10, 100);
        //===============================================================================
        return false;
    }

    //17_01_20 : try to correct bug =================================================

    //DOUBT : we center on the object for better detection
    turn_right_gyro(angle_obj_detected, 100);
    //===============================================================================

    chain_down(10);
    identify();
    //0:ball  1:cube  2:triangle up  3:cylindre  4:triangle down
    if (object_identified == 0)
    {
        chain_up(150);
        forward(true, 11, 200);
        kick_action();
        Sleep(200);
        kick_prepare();
    }
    else if (object_identified == 1)
    {
        //cube
        printf("cube: x=%f y=%f\n", loc_x + 10 + value_ultrasonic_save * sin(angle_obj_detected * M_PI / 180), loc_y + 10 + value_ultrasonic_save * cos(angle_obj_detected * M_PI / 180));
    }
    else if (object_identified == 2)
    {
        //triangle up
        printf("triangle up : x=%f y=%f\n", loc_x + 10 + value_ultrasonic_save * sin(angle_obj_detected * M_PI / 180), loc_y + 10 + value_ultrasonic_save * cos(angle_obj_detected * M_PI / 180));
    }
    else if (object_identified == 3)
    {
        //cylinder
        printf("cylinder: x=%f y=%f\n", loc_x + 10 + value_ultrasonic_save * sin(angle_obj_detected * M_PI / 180), loc_y + 10 + value_ultrasonic_save * cos(angle_obj_detected * M_PI / 180));
    }
    else
    {
        printf("triangle down: x=%f y=%f\n", loc_x + 10 + value_ultrasonic_save * sin(angle_obj_detected * M_PI / 180), loc_y + 10 + value_ultrasonic_save * cos(angle_obj_detected * M_PI / 180));
        //triangle down
    }

    //17_01_20 : try to correct bug =================================================

    //Now that we have detected the object we go back to initial angle
    turn_right_gyro(value_gyro - 10, 100);
    //===============================================================================
    chain_down(10);

    return true;
    //}
}

void follow_pattern()
{
    //Go around all the map to find objects
    //First  we take the initial value of gyro as reference
    float value_gyro;
    get_sensor_value0(sn2, &value_gyro);

    /* We won't use that finally as we need to cover all the map
       *
       *
      //Coord of all stops x and y
      //We will stop each 10cm
      //We don't want to go closer than 10cm from the wall to not disturb the function turn_until_find
      int nextstop=0;
      //We set the actual location at first
      int nextstepx=loc_x;
      int nextstepy=loc_y;
      int stops[][2]={
          {100,25},//right down corner
          {100,90},//right up corner
          {0,90},//up left corner
          {0,0},//down left corner
      };


       turn_right_gyro(value_gyro+90,100);
      while(nextstop<4)
      {
          bool obstacle;
          obstacle=check_if_obj_in_sight()
          //We start by looking for objects where we are
          //If there is an obstacle we have to contourn it
          //By the way we identify it
      }
       */

    //We will travel the map in a S shape
    //We stop at 10cm of the wall each time
    //We progress by step of 10 cm

    pthread_mutex_lock(&lock);
    float nextstepx = loc_x;
    float nextstepy = loc_y;

    pthread_mutex_unlock(&lock);
    int direction = 0;
    const int max_x = 85;
    const int min_x = 15;
    const int min_y = 15;
    const int max_y = 85;
    //The actual direction of the robot
    //0 for right
    //1 up
    //2 left

    //Based on value gyro, we calculate the right,up, down, left angles
    float up_angle = value_gyro;
    //Check for modulos : Bug?
    float left_angle = value_gyro - 90;
    float right_angle = value_gyro + 90;
    float down_angle = value_gyro + 180;
    int stepgoingup = 0;
    //17_01_20 : try to correct bug =================================================
    bool obj_pres_last_time = false;
    //===============================================================================

    while (nextstepy < max_y) //While we are not at the top
    {
        //HERE SCAN FOR OBJECT

        //17_01_20 : try to correct bug =================================================

        //Optimization : we only try to check for an object if no object was detected last time
        //Otherwise after detection it will look uselessly for an object a second time
        bool obj_pres;
        if (!obj_pres_last_time)
            obj_pres = check_if_obj_in_sight();
        else
            obj_pres = false;

        //===============================================================================

        //bool obj_pres = check_if_obj_in_sight();

        //17_01_20 : try to correct bug =================================================
        if (obj_pres && obj_pres_last_time == false)
        {
            //===============================================================================

            //if (obj_pres) {
            //we have to contourn it
            //Value_gyro points upward as it is the initial angle
            //We store the actual position

            //17_01_20 : try to correct bug =================================================
            obj_pres_last_time = true;
            //I think it will continue to detect object twice but at least it will move and not stay on place forever
            //===============================================================================

            float actualx = nextstepx;
            float actualy = nextstepy;

            //We want to know where the object has been detected

            //PROBLEM ; angle of the object :
            //We detect the angle of the middle of the object but the edge could collide with the robot moving
            //If the object is up or down we can continue our path
            if (abs(angle_obj_detected - up_angle) < 45)
            {
                //We do nothing except if we are going up
                if (direction == 1)
                {
                    //CONTOURN OBJECT
                    //If we are on the right side, we contourn the object by the left
                    if (nextstepx - ((max_x - min_x) / 2) > 0)
                    {
                        //We are on the right side,
                        nextstepx -= 20;
                        //We go a little bit on the left and we will go upstair from here
                    }
                    else
                    {
                        //we are on the left, we contourn by the right
                        nextstepx += 20;
                    }

                    /* Too complicate :
             *
             * if (abs(nextstepx - max_x) < 10)
                              {
                                  //We are on the right side, near the wall
                                  nextstepx-=20;
                                  //We go a little bit on the left and we will go upstair from here
                              }
                              else if (abs(nextstepx - min_x) < 10) {
                                  //we are on the left wall, we contourn by the right
                                  nextstepx+=20;
                              }
                              else if (nextstepx-((max_x-min_x)/2)>0)
                              {
                                  //We are on the right side but not near the wall
                                  //(we must be in a step different than 1 from the up movement)
                              }
          */

                    //at next iteration(if there are no object) we will go upstair
                    //For now we go to left or right to contourn the object
                    go_to_location(nextstepx, nextstepy);
                    //17_01_20 : try to correct bug =================================================
                    pthread_mutex_lock(&lock);
                    loc_x = nextstepx;
                    loc_y = nextstepy;
                    pthread_mutex_unlock(&lock);
                    //===============================================================================
                }
            }
            else if (abs(angle_obj_detected - down_angle) < 45)
            {
                //we do nothing
            }
            else if (abs(angle_obj_detected - left_angle) < 45)
            {
                //The object is on the left
                //If we are going on the right or up: no problem

                if (direction == 2)
                {
                    //CONTOURN OBJECT
                    //If we are near the left wall (30cm), we can simply go upstair
                    if (abs(nextstepx - min_x) < 30)
                    {
                        //We simply set the direction to up
                        direction = 1;
                    }
                    else
                    {
                        //difficult case :  we assume there are no object in our avoid pattern for now
                        //we go up, left for 30cm, and down again
                        nextstepy += 20;
                        go_to_location(nextstepx, nextstepy);
                        //17_01_20 : try to correct bug =================================================

                        pthread_mutex_lock(&lock);
                        loc_x = nextstepx;
                        loc_y = nextstepy;

                        pthread_mutex_unlock(&lock);
                        //===============================================================================

                        nextstepx -= 30;
                        go_to_location(nextstepx, nextstepy);
                        //17_01_20 : try to correct bug =================================================

                        pthread_mutex_lock(&lock);
                        loc_x = nextstepx;
                        loc_y = nextstepy;

                        pthread_mutex_unlock(&lock);
                        //===============================================================================

                        nextstepy -= 20;
                        go_to_location(nextstepx, nextstepy);
                        //17_01_20 : try to correct bug =================================================
                        pthread_mutex_lock(&lock);
                        loc_x = nextstepx;
                        loc_y = nextstepy;
                        pthread_mutex_unlock(&lock);
                        //===============================================================================
                    }
                }
            }
            else if (abs(angle_obj_detected - right_angle) < 45)
            {
                //The object is on the right
                //If we are going on the left or up: no problem

                if (direction == 0)
                {

                    //CONTOURN OBJECT
                    //If we are near the right wall (30cm), we can simply go upstair
                    if (abs(nextstepx - max_x) < 30)
                    {
                        //We simply set the direction to up
                        direction = 1;
                    }
                    else
                    {
                        //difficult case :  we assume there are no object in our avoid pattern for now
                        //we go up, right for 30cm, and down again
                        nextstepy += 20;
                        go_to_location(nextstepx, nextstepy);
                        //17_01_20 : try to correct bug =================================================
                        pthread_mutex_lock(&lock);
                        loc_x = nextstepx;
                        loc_y = nextstepy;
                        pthread_mutex_unlock(&lock);
                        //===============================================================================
                        nextstepx += 30;
                        go_to_location(nextstepx, nextstepy);
                        //17_01_20 : try to correct bug =================================================
                        pthread_mutex_lock(&lock);
                        loc_x = nextstepx;
                        loc_y = nextstepy;
                        pthread_mutex_unlock(&lock);
                        //===============================================================================

                        nextstepy -= 20;
                        go_to_location(nextstepx, nextstepy);
                        //17_01_20 : try to correct bug =================================================
                        pthread_mutex_lock(&lock);
                        loc_x = nextstepx;
                        loc_y = nextstepy;
                        pthread_mutex_unlock(&lock);
                        //===============================================================================
                    }
                }
            }
        }
        //IF NO OBJECT :
        else
        {
            //17_01_20 : try to correct bug =================================================
            obj_pres_last_time = false;
            //We reset that to false so next time it detects objects
            //===============================================================================

            if (direction == 0)
            //We are going to the right
            {
                if (nextstepx + 10 <= max_x)
                {
                    nextstepx += 10;
                }
                else //we have reached the wall
                {
                    direction = 1;
                    //We go up
                }
            }
            else if (direction == 2)
            {
                if (nextstepx - 10 >= min_x)
                {
                    nextstepx -= 10;
                }
                else //we have reached the wall
                {
                    direction = 1;
                    //We go up
                }
            }
            else if (direction == 1)
            {
                nextstepy += 10;
                //We go upstair of 30 to not scan multiple time same zone (old up becomes new down)
                if (stepgoingup < 2)
                {
                    stepgoingup++;
                }
                else
                {
                    stepgoingup = 0;
                    //old : abs(nextstepx - max_x) < 10)
                    if (nextstepx - ((max_x - min_x) / 2) > 0) //We are on the right side, we go to the left afterwards
                    {
                        direction = 2;
                    }
                    else
                    {
                        direction = 0;
                    }
                }
            }
            go_to_location(nextstepx, nextstepy);
            //17_01_20 : try to correct bug =================================================
            /*pthread_mutex_lock(&lock);
            loc_x = nextstepx;
            loc_y = nextstepy;
            pthread_mutex_unlock(&lock);*/
            //===============================================================================
        }
    }
}

//22_01_20 :

#ifndef M_PI
#define M_PI 3.141592
#endif

bool can_go_angle(float angle, float dist)
{
    float init_angle;
    //First we store the initial angle
    get_compass_value(&init_angle);
    //Check if we can go in the given angle or not
    //1st step  : we go to a certain angle left of the given direction, and we check until we are on a certainangle right if there is anything

    //Calculus :
    //    The robot is about 22cm width
    //If we want to move of *dist* in the *angle* direction, for our triangle : we want to have the adjacent side of length *dist*, and the opposite side of length 22/2=11
    //tan(wanted_angle)=11/dist
    float angle_wanted;
    angle_wanted = atan(11 / dist) * 180 / M_PI;

    //First we measure distance in the center
    turn_right_gyro(angle, 100);
    float dist_angle_center;
    get_sensor_value0(sn3, &dist_angle_center);

    turn_right_gyro(angle - angle_wanted, 100);
    float dist_angle_left;
    get_sensor_value0(sn3, &dist_angle_left);

    //the other side
    turn_right_gyro(angle + angle_wanted, 100);
    float dist_angle_right;
    get_sensor_value0(sn3, &dist_angle_right);
    float requested_dist_angle;
    //to be able to go forward of *dist*, the distance from any object in our direction must be superior to dist/cos(wanted-angle)
    requested_dist_angle = dist / cos(angle_wanted * M_PI / 180);
    bool ret_val;
    if (dist_angle_center < dist || dist_angle_right < requested_dist_angle || dist_angle_left < requested_dist_angle)
    {
        //If there is an object in the direction
        ret_val = false;
        dist_to_obj_angle = MIN(dist_angle_center, cos(angle_wanted * M_PI / 180) * MIN(dist_angle_left, dist_angle_right));
    }
    else
    {
        ret_val = true;
    }
    //we go back to the old angle before return
    turn_right_gyro(init_angle, 100);
    return ret_val;
}

void follow_simpler_path()
{
    //For this path, we go across each line at maximum until we find an object or a wall
    //Then we go the next y, 30cm up
    float nextstepx = loc_x;
    float nextstepy = loc_y;

    float init_angle;
    //First we store the initial angle
    //It should be the up direction
    get_compass_value(&init_angle);

    //int direction = 0;
    //0 right 1 left 2up
    const int max_x = 100;
    const int min_x = 0;
    const int min_y = 0;
    const int max_y = 90;

    int max_x_given_y = max_x;
    int min_x_given_y = min_x;

    //Based on value gyro, we calculate the right,up, down, left angles
    float up_angle = init_angle;

    float left_angle = init_angle - 90;
    float right_angle = init_angle + 90;
    float down_angle = (float)(((int)(init_angle) + 180) % 360);

    while (nextstepy < max_y)
    {
        /*if(direction!=2)//We don't want to goup for now
		{
			
		}*/
        max_x_given_y = max_x;
        min_x_given_y = min_x;

        //First, we want to check the right side of the line*
        //We check if it is possible to go to the right wall; otherwise it means there is an object
        bool rightok = can_go_angle(right_angle, max_x - nextstepx);
        if (!rightok)
        {
            //			We have to go and identify the object
            //We stop at 15cm from the object
            max_x_given_y = nextstepx + dist_to_obj_angle;
            nextstepx = nextstepx + dist_to_obj_angle - 15;
            go_to_location(nextstepx, nextstepy);
            check_if_obj_in_sight();
        }
        //Now we check the left side
        bool leftok = can_go_angle(right_angle, nextstepx - min_x);
        if (!leftok)
        {
            //			We have to go and identify the object
            //We stop at 15cm from the object
            nextstepx = nextstepx - dist_to_obj_angle + 15;
            min_x_given_y = nextstepx - dist_to_obj_angle;
            go_to_location(nextstepx, nextstepy);
            check_if_obj_in_sight();
        }
        //Now we should have finished for this line, we want to go up of 30cm to check for the next line
        bool upok = can_go_angle(up_angle, 30);
        if (!upok)
        {
            //If we can't go up we try again from max right to max left
            nextstepx = max_x_given_y - 30;
            go_to_location(nextstepx, nextstepy);
            upok = can_go_angle(up_angle, 30);
        }
        while (!upok && nextstepx > min_x_given_y)
        {
            /*//We go left or right depending on our current position and try again
			bool go_left = (nextstepx > (max_x_given_y-min_x_given_y)/2);
			//We prefer left if we are on the right
			nextstepx+=(go_left)*/
            nextstepx -= 30;
            go_to_location(nextstepx, nextstepy);
            upok = can_go_angle(up_angle, 30);
        }
        if (!upok)
        {
            printf("error!!!!!!!!!!!!!!!!!!!!\n");
            return;
        }
        nextstepy += 30;
        go_to_location(nextstepx, nextstepy);
    }
}

//Initial code :
/*

#define LEFT_LIMIT 0
#define RIGHT_LIMIT 120
#define DOWN_LIMIT 0
#define UP_LIMIT 100

#define EPSILON    (1.0E-8)
//Error range : for estimating if we see a wall or an object, we accept an error of 3cm
#define ERROR_RANGE 3
//Continue range : range under which we stop going forward if what we see is a wall : 20cm
#define CONT_RANGE 20

#define WIDTH_ROBOT 35
#define MIN_X_ROBOT LEFT_LIMIT+WIDTH_ROBOT/2
#define MAX_X_ROBOT RIGHT_LIMIT-WIDTH_ROBOT/2


#define LENGTH_ROBOT 35
#define MIN_Y_ROBOT DOWN_LIMIT+LENGTH_ROBOT/2
#define MAX_Y_ROBOT UP_LIMIT-LENGTH_ROBOT/2 //(we don't want to go to opposite side)


//The position of the robot on the grid
//From the perspective of our robot : y=LENGTH_ROBOT/2 is the start position
//x_initial = 120/2=60, x=0 is on the left of the robot when it goes forward (forward=to y increasing)
//float positionRobotX;
//Between x=MIN_X_ROBOT and x=MAX_X_ROBOT
//float positionRobotY;
//Between y= MIN_Y_ROBOT and y=MAX_Y_ROBOT     (we don't want to go to opposite side)
//float robotAngle;
//between -180 and 180 excluded , the angle of the robot 0 being the y axis




//NOTE TO MYSELF :
// 2 VARIABLES TO RETURN :
//The first in parameter with & is an indicator for the main function : 0 if nothing has changed so the robot keeps going forward, 1 if there is a new order to follow
//The second parameter is the return parameter can be Forward, left, right, object, (backward?) (coded into an int?)

//Values returned :
// 0 : continue forward
//1 : go left
//2 : go right
//3 : go backward
// 4 : there is an object



float distanceToLimit(float distanceGivenByUS, float positionRobotX, float positionRobotY, float robotAngle)
{
	//The robot trajectory vector is :
	float robotVectorX = sin(robotAngle*M_PI/180);
	float robotVectorY = cos(robotAngle*M_PI/180);

}


int isObjectInSight(float distanceGivenByUS, float positionRobotX, float positionRobotY, float robotAngle)
{
	//Decide if what we have in sight is an object or not



	//First, giving the angle, we calculate the distance of our robot to the limit of our field in its current direction, given it's position
	//float distance = distanceToLimit( distanceGivenByUS, positionRobotX, positionRobotY,  robotAngle);
	////==========The distanceCalculus
	//The robot trajectory vector is :
	float robotVectorX = sin(robotAngle*M_PI/180);
	float robotVectorY = cos(robotAngle*M_PI/180);
	//Considering a variable t, we search for the smallest t of time of collision for each wall
	//Wall 1 : left, x=LEFT_LIMIT, wall 2 : up (the line) y=UP_LIMIT, wall 3 : right, x=RIGHT_LIMIT, wall 4 (or0) : down, y=DOWN_LIMIT ;
	float tWall1;
	float tWall2;
	float tWall3;
	float tWall4;
	if(abs(robotVectorX) > EPSILON)
	{
		tWall1 = -(LEFT_LIMIT-positionRobotX)/robotVectorX;
		tWall3 = (RIGHT_LIMIT-positionRobotX)/robotVectorX;

	}
	else
	{
		tWall1 = FLT_MAX:
		tWall3 = FLT_MAX:
	}
	if(abs(robotVectorY) > EPSILON)
	{
		tWall2 = (UP_LIMIT-positionRobotY)/robotVectorY;
		tWall4 = -(DOWN_LIMIT-positionRobotY)/robotVectorY;

	}
	else
	{
		tWall2 = FLT_MAX:
		tWall4 = FLT_MAX:
	}
	//The negative times are set to INT_MAX (because we are getting away from the wall)
	tWall1=tWall1<0?INT_MAX:tWall1;
	tWall2=tWall1<0?INT_MAX:tWall2;
	tWall3=tWall1<0?INT_MAX:tWall3;
	tWall4=tWall1<0?INT_MAX:tWall4;
	
	
	//Now we have the arrival time at each wall
	//We distinguish 4 cases according to the robot heading
	//angle between -180 and 180
	//angle +180 between 0 and 360
	//(int)((angle+180)/90) is 0 1 2 or 3
	//to prevent a possible error if angle is 180, we could take the mod 4
	int decisionAngle = (int)((robotAngle+180)/90);
	//The variable that contains the distance to the wall that is on the current path of the robot :
	double distToPathWall;
	//The number of the wall on the path
	int wallNb;
	switch(decisionAngle)
	{
		case 4:
		case 0:
		//Either the down wall 0 or the left wall 1
		distToWallPath=tWall1<tWall4?(positionRobotX-LEFT_LIMIT)/cos((robotAngle+90)*M_PI/180):(positionRobotY-DOWN_LIMIT)/cos((180+robotAngle)*M_PI/180);
		
		wallNb=tWall1<tWall4?1:0;
		break;
		case 1:
		//Either the up wall 2 or the left wall 1
		distToWallPath=tWall1<tWall2?(positionRobotX-LEFT_LIMIT)/cos((90+robotAngle)*M_PI/180):(UP_LIMIT-positionRobotY)/cos(robotAngle);
		
		wallNb=tWall1<tWall2?1:2;
		break;
		case 2:
		//Either the up wall 2 or the right wall 3
		distToWallPath=tWall3<tWall2?(RIGHT_LIMIT-positionRobotX)/cos((90-robotAngle)*M_PI/180):(UP_LIMIT-positionRobotY)/cos(robotAngle*M_PI/180);
		//For instance for the first one : The distance to wall on X scale, divided the cos of the angle to get the total distance.
		//It is a rectangular triangle, we want the hypotenus
		wallNb=tWall3<tWall2?3:2;
		break;
		case 3:
		//Either the down wall 0 or the right wall 3
		distToWallPath=tWall3<tWall4?(RIGHT_LIMIT-positionRobotX)/cos((robotAngle-90)*M_PI/180):(positionRobotY-DOWN_LIMIT)/cos((180-robotAngle)*M_PI/180);
		
		wallNb=tWall3<tWall4?3:0;
		break;
	}
	//=====================================================
	//Useless code for now: to know the minimal distance to the next wall  from the robot 
	double minDistToWallPath;
	switch(decisionAngle)
	{
		case 4:
		case 0:
		//Either the down wall 0 or the left wall 1
		minDistToWallPath=tWall1<tWall4?positionRobotX-LEFT_LIMIT:positionRobotY-DOWN_LIMIT;
		break;
		case 1:
		//Either the up wall 2 or the left wall 1
		minDistToWallPath=tWall1<tWall2?positionRobotX-LEFT_LIMIT:UP_LIMIT-positionRobotY;
		break;
		case 2:
		//Either the up wall 2 or the right wall 3
		minDistToWallPath=tWall3<tWall2?RIGHT_LIMIT-positionRobotX:UP_LIMIT-positionRobotY;
		break;
		case 3:
		//Either the down wall 0 or the right wall 3
		minDistToWallPath=tWall3<tWall4?RIGHT_LIMIT-positionRobotX:positionRobotY-DOWN_LIMIT;
		break;
	}
	
	//==================================================
	//Now that we have the distance to the next wall, we know if there is an object or if it is the wall
	
	//If there is an object :
	if(distToWallPath>distanceGivenByUS+ERROR_RANGE)
		return 4;
	
	//If no object and no wall soon we continue forward
	if(distToWallPath>CONT_RANGE)
		return 0;

	//Now to determine if we go left or right :
	int retValue; //left 1 or right 2
	switch(wallNb)
	{
		case 0:
		retValue=positionRobotX-LEFT_LIMIT>(RIGHT_LIMIT-LEFT_LIMIT)/2?2:1;
		break;
		case 1:
		retValue=positionRobotY-DOWN_LIMIT>(UP_LIMIT-DOWN_LIMIT)/2?1:2;
		break;
		case 2:
		retValue=positionRobotX-LEFT_LIMIT>(RIGHT_LIMIT-LEFT_LIMIT)/2?1:2;
		break;
		case 3:
		retValue=positionRobotY-DOWN_LIMIT>(UP_LIMIT-DOWN_LIMIT)/2?2:1;
		break;
		
	}
	return retValue;
	


}



*/

//JEAN & PRINCE ENDS

void last_chance_path()
{

    get_compass_value(&value_gyro);
    //Based on value gyro, we calculate the right,up, down, left angles
    float up_angle = value_gyro;

    float left_angle = (float)(((int)value_gyro - 90) % 360);
    float right_angle = (float)(((int)value_gyro + 90) % 360);
    float down_angle = (float)(((int)value_gyro + 180) % 360);

    const int max_x = 85;
    const int min_x = 15;
    const int min_y = 15;
    const int max_y = 85;

    int nextstepx = loc_x;
    int nextstepy = loc_y;
    nextstepy += 10;
    //Move from the wall to avoid wrong detection
    go_to_location(nextstepx, nextstepy);
    //We check if there is an object
    check_if_obj_in_sight();

    //We loop until the end of the game
    while (true)
    {

        //We choose a random direction
        int r;
        bool dirAvailable = false;
        while (!dirAvailable)
        {
            r = rand() % 4;
            switch (r)
            {

            //We try each 4 directions

            //If we are not too close to a wall or if there are no object in that direction, we stop looping and go forward
            case 0:
                //up
                if (nextstepy < max_y - 15)
                {

                    dirAvailable = can_go_angle(up_angle, 15);

                    if (dirAvailable)
                    {
                        nextstepy += 15;
                    }
                }

                break;

            case 1:
                //down
                if (nextstepy > min_y + 15)
                {

                    dirAvailable = can_go_angle(down_angle, 15);

                    if (dirAvailable)
                    {
                        nextstepy -= 15;
                    }
                }

                break;

            case 2:
                //right
                if (nextstepx < max_x - 15)
                {

                    dirAvailable = can_go_angle(right_angle, 15);

                    if (dirAvailable)
                    {
                        nextstepx += 15;
                    }
                }

                break;

            case 3:
                //left
                if (nextstepx > min_x + 15)
                {

                    dirAvailable = can_go_angle(left_angle, 15);

                    if (dirAvailable)
                    {
                        nextstepx -= 15;
                    }
                }

                break;
            }
        }
        go_to_location(nextstepx, nextstepy);
        //We go to location and check for object
        check_if_obj_in_sight();
    }
}

//End 29&30 01 20

//SERVER STUFF PRINCE
// This reads from 0 to maxSize bits from server and it stores it in buffer
int read_from_server(int sock, uint8_t *buffer, size_t maxSize)
{

    int bytes_read = read(sock, buffer, maxSize);

    if (bytes_read <= 0)
    {
        fprintf(stderr, "[ERR] Server unexpectedly closed connection...\n");
        close(serv);
        exit(EXIT_FAILURE);
    }

    printf("[DEBUG] Received %d bytes\n", bytes_read);
    return bytes_read;
}

void funct(int sockfd, uint8_t *buffer, int id)
{
    printf("[DEBUG_FUNCT] Ready to send a message\n");
    write(sockfd, buffer, 8);

    printf("[DEBUG_FUNCT] Ready to receive an ACK\n");

    read_from_server(sockfd, buffer, 8);
    //uint16_t id = buffer[0] | buffer[1] << 8; //i don't care about ack id
    uint8_t src = buffer[2];
    uint8_t dst = buffer[3];
    uint8_t type = buffer[4];
    uint8_t error = buffer[5];
    uint16_t idConfirmed = buffer[6] | buffer[7] << 8;

    if (src != 255)
    {
        printf("[DEBUG_FUNCT_ERR] I expected a message from the server (id=255), but I received a message from a foreigner (id sender = %d)\n", src);
        free(buffer);
        exit(65);
    }

    if (dst != TEAM_ID)
    {
        printf("[DEBUG_FUNCT_ERR] I expected a message addressed to me (id = %d), but I wasn't the receiver (id receiver = %d)\n", TEAM_ID, dst);
        free(buffer);
        exit(75);
    }

    if (type != 0)
    {
        printf("[DEBUG_FUNCT_ERR] I expected an ACK (type=0), but I received another type of message\n");
        free(buffer);
        exit(55);
    }

    if (error > 0)
    {
        printf("[DEBUG_FUNCT_ERR] My last message was not well processed: %d\n", error);
        free(buffer);
        exit(85);
    }

    if (idConfirmed != id)
    {
        printf("[ERR] ID issue\n");
        free(buffer);
        exit(95);
    }

    printf("[DEBUG_FUNCT] ACK received\n");

    free(buffer);
}

uint8_t *buffer_msg_obj_id(uint16_t id, int obj_type, int obj_x, int obj_y)
{
    uint8_t *buffer = (uint8_t *)malloc(8 * sizeof(uint8_t));

    if (buffer == NULL)
    {
        printf("[ERR] Memory finished\n");
    }

    buffer[0] = (uint8_t)id;
    buffer[1] = (uint8_t)(id >> 8);
    buffer[2] = TEAM_ID;    //src, us
    buffer[3] = 255;        //dst, server (id=255)
    buffer[4] = MSG_OBJ_ID; //Message type
    buffer[5] = obj_type;   //8 -> cylinder
    buffer[6] = obj_x;      //x
    buffer[7] = obj_y;      //y

    return buffer;
}

uint8_t *buffer_msg_kick(uint16_t id, int ball_x, int ball_y)
{
    uint8_t *buffer = (uint8_t *)malloc(8 * sizeof(uint8_t));

    if (buffer == NULL)
    {
        printf("[ERR] Memory finished\n");
    }

    buffer[0] = (uint8_t)id;
    buffer[1] = (uint8_t)(id >> 8);
    buffer[2] = TEAM_ID;  //src, us
    buffer[3] = 255;      //dst, server (id=255)
    buffer[4] = MSG_KICK; //Message type
    buffer[5] = ball_x;   //x
    buffer[6] = ball_y;   //y
    buffer[7] = 0;        //random bits

    return buffer;
}

uint8_t *buffer_msg_test(uint16_t id)
{
    uint8_t *buffer = (uint8_t *)malloc(8 * sizeof(uint8_t));

    if (buffer == NULL)
    {
        printf("[ERR] Memory finished\n");
    }

    buffer[0] = (uint8_t)id;
    buffer[1] = (uint8_t)(id >> 8);
    buffer[2] = TEAM_ID;  //src, us
    buffer[3] = 255;      //dst, server (id=255)
    buffer[4] = MSG_TEST; //Message type
    buffer[5] = 0;
    buffer[6] = 0;
    buffer[7] = 0;

    return buffer;
}

void *send_msg_thread(void *vargp)
{
    //struct arg_msg_struct *args = (struct arg_msg_struct *)vargp;
    struct arg_msg_struct *args = (struct arg_msg_struct *)vargp;

    int sockfd = args->arg_sockfd;
    int msg_type = args->arg_msg_type;
    int obj_type = args->arg_obj_type;
    int x = args->arg_x;
    int y = args->arg_y;

    uint8_t *buffer = NULL;
    uint16_t id = 11000;

    pthread_mutex_lock(&lockMsg);

    if (msg_type == MSG_KICK)
    {
        id = 12000;
        buffer = buffer_msg_kick(id, x, y);
    }
    else if (msg_type == MSG_OBJ_ID)
    {
        id = 13000;
        buffer = buffer_msg_obj_id(id, obj_type, x, y);
    }
    else
    {
        //test msg
        buffer = buffer_msg_test(id);
    }

    funct(sockfd, buffer, id);
    sleep(10);

    pthread_mutex_unlock(&lockMsg);

    return NULL;
}

int init_queri(int sockfd)
{
    //Receive the start message and print the various parameters
    uint8_t *buffer = (uint8_t *)malloc(8 * sizeof(uint8_t));
    if (buffer == NULL)
    {
        printf("[INIT_QUERY_ERR] Memory finished\n");
    }
    read_from_server(sockfd, buffer, 8);

    uint16_t id = buffer[0] | buffer[1] << 8;

    uint8_t src = buffer[2];
    uint8_t dst = buffer[3];
    uint8_t type = buffer[4];

    printf("[INIT_QUERY] ID of the start: %d\n", id);
    printf("[INIT_QUERY] From: %d\n", src);
    printf("[INIT_QUERY] To: %d\n", dst);
    printf("[INIT_QUERY] Type: %d\n", type);

    if (src != 255)
    {
        printf("[INIT_QUERY_ERR] I expected a message from the server (id=255), but I received a message from a foreigner\n");
        exit(65);
    }

    if (dst != TEAM_ID)
    {
        printf("[INIT_QUERY_ERR] I expected a message addressed to me (id = %d), but I wasn't the receiver (id receiver = %d)\n", TEAM_ID, dst);
        exit(75);
    }

    return type;
}

void *request_start_thread(void *vargp)
{
    struct arg_msg_struct *args = (struct arg_msg_struct *)vargp;

    int sockfd = args->arg_sockfd;

    //uint8_t *type = init_queri(int sockfd);
    int type = init_queri(sockfd);

    args->connect_type = type;

    return NULL;
}
//SERVER STUFF ENDS

//to implement:
//Location update Thread: implement thread calculating location always using encoder -  Sanjeev - JF
//Obstacles: thread running, when find an object, stop and try to identify it - Adib
//fix first rotation at 1st sec when starting to move- maybe raise ramping value - ALL
//Threads: implement and test threads - Prince
//Implement main function - Prince
//website
/*

Test #1. Be able to find an object in your side (without identifying its shape). For this test, only one object is put in your side.
Test #2. Be able to identify an object positioned in your side. For this test, only one object is placed in your side.
Test #3. Be able to identify an object and estimate its position.
Test #4. Be able to find a ball located at a predefined position.
DONE  Test #5. Be able to shoot a ball in the opposite side. You can place your robot and the ball as you wish.
LATER Test #6. Be able to connect to the server and inform the server when an object was detected.
*/

int main(void)
{
    //struct s_move_args *move_args=malloc(sizeof(struct s_move_args));
    //pthread_t threads[10];
    //#ifndef __ARM_ARCH_4T__
    /* Disable auto-detection of the brick (you have to set the correct address below) */
    /*ev3_brick_addr = "192.168.0.204";
  #endif
    if ( ev3_init() == -1 ) return ( 1 );
  #ifndef __ARM_ARCH_4T__
    printf( "The EV3 brick auto-detection is DISABLED,\nwaiting %s online with plugged tacho...\n", ev3_brick_addr );
  #else
    printf( "Waiting tacho is plugged...\n" );
  #endif*/

    //SERVER STUFF
    /*struct sockaddr_in servaddr, cli;

    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        printf("[ERR] Socket creation failed...\n");
        exit(0);
    }
    else
        printf("[DEBUG] Socket successfully created..\n");
    bzero(&servaddr, sizeof(servaddr));

    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(SERV_ADDR);
    servaddr.sin_port = htons(SERV_PORT);

    // connect the client socket to server socket
    if (connect(sockfd, (SA *)&servaddr, sizeof(servaddr)) != 0)
    {
        printf("[ERR] Connection with the server failed...\n");
        exit(0);
    }
    else
        printf("[DEBUG] Connected to the server..\n");

    //listen for start
    printf("listen to the server\n");
    msg_args.arg_sockfd = sockfd;
    if (pthread_create(&connect_thread, NULL, request_start_thread, (void *)&msg_args) != 0)
    {
        printf("connect thread fail\n");
        return -1;
    }
    pthread_join(connect_thread, NULL);

    if (msg_args.connect_type == MSG_START)
    {
        printf("[DEBUG] START message received\n");

        //ROBOT INIT
        while (ev3_tacho_init() < 1)
            Sleep(1000);
        printf("*** ( EV3 ) Hello! ***\n");
        //Motor INITIALISATION
        motors_init_task();
        //SENSOR INITIALISATION
        ev3_sensor_init();
        sensors_init_task();
        //start_location_updating_thread();
        check_if_obj_in_sight();
        turn_right_gyro(180, 100);
        check_if_obj_in_sight();

        //go_to_location(70, 25);
    }
    else if (msg_args.connect_type == MSG_STOP)
    {
        printf("[DEBUG] STOP message received\n");
    }
    else
    {
        printf("[ERR] I expected a START message, but another message arrived\n");
        exit(55);
    }*/

    /*printf("Sending msg to the server\n");
    msg_args.arg_sockfd = sockfd;
    msg_args.arg_msg_type = MSG_OBJ_ID;
    msg_args.arg_obj_type = OBJ_CUBE;
    msg_args.arg_x = 10;
    msg_args.arg_y = 20;

    //pthread_create(&send_thread, NULL, send_msg_thread, (void *)&msg_args);
    if (pthread_create(&send_thread, NULL, send_msg_thread, (void *)&msg_args) != 0)
    {
        printf("send thread fail\n");
        return -1;
    }
    pthread_join(send_thread, NULL);*/

    //ROBOT INIT
    while (ev3_tacho_init() < 1)
        Sleep(1000);
    printf("*** ( EV3 ) Hello! ***\n");
    //Motor INITIALISATION
    motors_init_task();
    //SENSOR INITIALISATION
    ev3_sensor_init();
    sensors_init_task();

    /*srand(time(NULL));

    last_chance_path();*/

    //go_to_location_dummy(50, 35);
    //identify_object();
    //identify();
    check_if_obj_in_sight();
    /*turn_right_gyro(180, 100);
    check_if_obj_in_sight();*/

    /*
    c=0;
    follow path && sensor value >20
    
    */
    //start_location_updating_thread();

    // while(1){
    //     //50,25
    //     go_to_location(x+10,y);
    //     check_ifobject
    //     go_to_location(x,y+10);
    // }
    /*
    turn_left_gyro(-90, 50);
    forward(true, abs(45), 200);
    turn_right_gyro(0, 50);
    forward(true, abs(20), 100);
    kick_action();*/
    // forward()
    //     go_to_location(45, 0);
    //     get_sensor_value0(sn2, &value_gyro);
    //     if (value_gyro < 15)
    //     {
    //         identify();
    //     }
    //     go_to_location(40, 0);
    //     get_sensor_value0(sn2, &value_gyro);
    //     if (value_gyro < 15)
    //     {
    //         identify();
    //     }
    //     go_to_location(35, 0);
    //     get_sensor_value0(sn2, &value_gyro);
    //     if (value_gyro < 15)
    //     {
    //         identify();
    //     }
    //     go_to_location(30, 0);
    //     get_sensor_value0(sn2, &value_gyro);
    //     if (value_gyro < 15)
    //     {
    //         identify();
    //     }
    //     go_to_location(25, 0);
    //     get_sensor_value0(sn2, &value_gyro);
    //     if (value_gyro < 15)
    //     {
    //         identify();
    //     }
    //     go_to_location(20, 0);
    //     get_sensor_value0(sn2, &value_gyro);
    //     if (value_gyro < 15)
    //     {
    //         identify();
    //     }
    //     go_to_location(10, 0);
    //     turn_right_gyro(0, 100);
    //     go_to_location(45, 30);
    //     get_sensor_value0(sn2, &value_gyro);
    //     if (value_gyro < 15)
    //     {
    //         identify();
    //     }
    //     go_to_location(45, 35);
    //     get_sensor_value0(sn2, &value_gyro);
    //     if (value_gyro < 15)
    //     {
    //         identify();
    //     }
    //     go_to_location(45, 40);
    //     get_sensor_value0(sn2, &value_gyro);
    //     if (value_gyro < 15)
    //     {
    //         identify();
    //     }
    //     go_to_location(45, 45);
    //     get_sensor_value0(sn2, &value_gyro);
    //     if (value_gyro < 15)
    //     {
    //         identify();
    //     }
    //     kick_action();
    // turn_right_gyro(250, 100);
    // forward(true, abs(10), 200);
    // turn_left_gyro(50, 100);
    // forward(true, abs(10), 200);
    // turn_right_gyro(250, 100);
    // forward(true, abs(10), 200);
    // turn_left_gyro(50, 100);
    // forward(true, abs(10), 200);
    // turn_right_gyro(250, 100);
    // forward(true, abs(10), 200);
    // turn_left_gyro(50, 100);
    // forward(true, abs(10), 200);
    // turn_right_gyro(250, 100);
    // forward(true, abs(10), 200);
    // turn_left_gyro(50, 100);
    // forward(true, abs(10), 200);
    //go_to_location(70, 25);
    //go_to_location(80, 35);

    //follow_simpler_path();

    //follow_pattern();
    /*set_tacho_position(sn_base[0], 75);
    set_tacho_position(sn_base[1], 75);
    start_location_updating_thread();
    go_to_location(60, 25);
    printf("aa\n");
    //Sleep(1000);
    //printf("a");
    go_to_location(70, 25);*/
    //printf("b");
    //start_find_obsctales();

    fflush(stdout);
    //while (1)
    //  ;

    //SEND MSG THREAD
    /*if (pthread_create(&send_thread, NULL, sendMsg, (void *)&msg_args) != 0)
    {
        printf("send thread fail\n");
        return -1;
    }*/

    ev3_uninit();
    printf("*** ( EV3 ) Bye! ***\n");

    return (0);
}
