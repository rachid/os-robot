#include "dummyserver.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdint.h>
#include <netdb.h>
#include <string.h>

int s;
uint16_t msgId = 0;

// This reads from 0 to maxSize bits from server and it stores it in buffer
int read_from_server(int sock, uint8_t *buffer, size_t maxSize)
{

    int bytes_read = read(sock, buffer, maxSize);

    if (bytes_read <= 0)
    {
        fprintf(stderr, "[ERR] Server unexpectedly closed connection...\n");
        close(s);
        exit(EXIT_FAILURE);
    }

    printf("[DEBUG] Received %d bytes\n", bytes_read);
    return bytes_read;
}

void funct(int sockfd, uint8_t *buffer, int id)
{
    printf("[DEBUG] Ready to send a message\n");
    write(sockfd, buffer, 8);

    printf("[DEBUG] Ready to receive an ACK\n");

    read_from_server(sockfd, buffer, 8);
    //uint16_t id = buffer[0] | buffer[1] << 8; //i don't care about ack id
    uint8_t src = buffer[2];
    uint8_t dst = buffer[3];
    uint8_t type = buffer[4];
    uint8_t error = buffer[5];
    uint16_t idConfirmed = buffer[6] | buffer[7] << 8;

    if (src != 255)
    {
        printf("[ERR] I expected a message from the server (id=255), but I received a message from a foreigner (id sender = %d)\n", src);
        free(buffer);
        exit(65);
    }

    if (dst != TEAM_ID)
    {
        printf("[ERR] I expected a message addressed to me (id = %d), but I wasn't the receiver (id receiver = %d)\n", TEAM_ID, dst);
        free(buffer);
        exit(75);
    }

    if (type != 0)
    {
        printf("[ERR] I expected an ACK (type=0), but I received another type of message\n");
        free(buffer);
        exit(55);
    }

    if (error > 0)
    {
        printf("[ERR] My last message was not well processed: %d\n", error);
        free(buffer);
        exit(85);
    }

    if (idConfirmed != id)
    {
        printf("[ERR] ID issue\n");
        free(buffer);
        exit(95);
    }

    printf("[DEBUG] ACK received\n");

    printf("[DEBUG] Ready to receive a STOP\n");

    read_from_server(sockfd, buffer, 8);
    //uint16_t id = buffer[0] | buffer[1] << 8; //i don't care about ack id
    src = buffer[2];
    dst = buffer[3];
    type = buffer[4];
    // the rest is random

    if (src != 255)
    {
        printf("[ERR] I expected a message from the server (id=255), but I received a message from a foreigner (id sender = %d)\n", src);
        free(buffer);
        exit(65);
    }

    if (dst != TEAM_ID)
    {
        printf("[ERR] I expected a message addressed to me (id = %d), but I wasn't the receiver (id receiver = %d)\n", TEAM_ID, dst);
        free(buffer);
        exit(75);
    }

    if (type != 2)
    {
        printf("[ERR] I expected a STOP (type=2), but I received another type of message\n");
        free(buffer);
        exit(55);
    }

    printf("[DEBUG] STOP received\n");
    free(buffer);
}

uint8_t *buffer_msg_obj_id(uint16_t id, int obj_type, int obj_x, int obj_y)
{
    uint8_t *buffer = (uint8_t *)malloc(8 * sizeof(uint8_t));

    if (buffer == NULL)
    {
        printf("[ERR] Memory finished\n");
    }

    buffer[0] = (uint8_t)id;
    buffer[1] = (uint8_t)(id >> 8);
    buffer[2] = TEAM_ID;    //src, us
    buffer[3] = 255;        //dst, server (id=255)
    buffer[4] = MSG_OBJ_ID; //Message type
    buffer[5] = obj_type;   //8 -> cylinder
    buffer[6] = obj_x;      //x
    buffer[7] = obj_y;      //y

    return buffer;
}

uint8_t *buffer_msg_kick(uint16_t id, int ball_x, int ball_y)
{
    uint8_t *buffer = (uint8_t *)malloc(8 * sizeof(uint8_t));

    if (buffer == NULL)
    {
        printf("[ERR] Memory finished\n");
    }

    buffer[0] = (uint8_t)id;
    buffer[1] = (uint8_t)(id >> 8);
    buffer[2] = TEAM_ID;  //src, us
    buffer[3] = 255;      //dst, server (id=255)
    buffer[4] = MSG_KICK; //Message type
    buffer[5] = ball_x;   //x
    buffer[6] = ball_y;   //y
    buffer[7] = 0;        //random bits

    return buffer;
}

uint8_t *buffer_msg_test(uint16_t id)
{
    uint8_t *buffer = (uint8_t *)malloc(8 * sizeof(uint8_t));

    if (buffer == NULL)
    {
        printf("[ERR] Memory finished\n");
    }

    buffer[0] = (uint8_t)id;
    buffer[1] = (uint8_t)(id >> 8);
    buffer[2] = TEAM_ID;  //src, us
    buffer[3] = 255;      //dst, server (id=255)
    buffer[4] = MSG_TEST; //Message type
    buffer[5] = 0;
    buffer[6] = 0;
    buffer[7] = 0;

    return buffer;
}

void *sendMsg(void *vargp)
{
    struct arg_msg_struct *args = (struct arg_msg_struct *)vargp;

    int sockfd = args->arg_sockfd;
    int msg_type = args->arg_msg_type;
    int obj_type = args->arg_obj_type;
    int x = args->arg_x;
    int y = args->arg_y;

    uint8_t *buffer = NULL;
    uint16_t id = 17054; //id will be updated

    pthread_mutex_lock(&lockMsg);

    if (msg_type == MSG_KICK)
    {
        buffer = buffer_msg_kick(id, x, y);
    }
    else if (msg_type == MSG_OBJ_ID)
    {
        buffer = buffer_msg_obj_id(id, obj_type, x, y);
    }
    else
    {
        //test msg
        buffer = buffer_msg_test(id);
    }

    funct(sockfd, buffer, id);
    sleep(10);

    pthread_mutex_unlock(&lockMsg);

    return NULL;
}
