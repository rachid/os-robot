#ifndef DUMMYTHREAD_H
#define DUMMYTHREAD_H
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdint.h>
#include <netdb.h>
#include <string.h>
#include <pthread.h>

#define SERV_ADDR "0.0.0.0"
#define SERV_PORT 1024
#define TEAM_ID 1 /* Your team ID */

#define MSG_ACK 0
#define MSG_START 1
#define MSG_STOP 2
#define MSG_KICK 3
#define MSG_OBJ_ID 4
#define MSG_TEST 5

//object type Prince AMANKWAH
#define OBJ_CUBE 1
#define OBJ_PYRMD 2
#define OBJ_PYRMD_INVRS 3
#define OBJ_PYRMD_QUAD 4
#define OBJ_PYRMD_TRIP 5
#define OBJ_PYRMD_QUAD_INVRS 6
#define OBJ_PYRMD_TRIP_INVRS 7
#define OBJ_CYL 8

#define SA struct sockaddr
#define Sleep(msec) usleep((msec)*1000)

int read_from_server(int sock, uint8_t *buffer, size_t maxSize);

void funct(int sockfd, uint8_t *buffer, int id);

uint8_t *buffer_msg_obj_id(uint16_t id, int obj_type, int obj_x, int obj_y);

uint8_t *buffer_msg_kick(uint16_t id, int ball_x, int ball_y);

uint8_t *buffer_msg_test(uint16_t id);

int init_queri(int sockfd);

void *send_msg_thread(void *vargp);

void *request_start_thread(void *vargp);

#endif